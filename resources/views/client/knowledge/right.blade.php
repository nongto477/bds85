<div class="right nth-feature">
    <ul class="overHide">
        @foreach ($rightKnow as $item)
            <li class="item">
                <div class="postImg">
                    <a
                        href="{{ route('article.detail',['id' => $item['id'], 'slug' => $item['slug']]) }}">
                        <img src="{{ asset('images/articles/x400/'.$item['thumbnail']) }}"
                            alt="{{ $item['title'] }}" />
                    </a>
                </div>
                <div class="text">
                    <h4>
                        <a
                            href="{{ route('article.detail',['id' => $item['id'], 'slug' => $item['slug']]) }}">
                            {{ $item['title'] }}
                        </a>
                    </h4>
                    <div class="desc">
                        {!! $item['description'] !!}
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
</div>