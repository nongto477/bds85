@foreach ($leftKnow as $item)
    <div class="feature-category">
        <div class="postImg">
            <a
                href="{{ route('article.detail',['id' => $item['id'], 'slug' => $item['slug']]) }}">
                <img src="{{ asset('images/articles/x400/'.$item['thumbnail']) }}"
                    alt="{{ $item['title'] }}" />
            </a>
        </div>
        <h4>
            <a
                href="{{ route('article.detail',['id' => $item['id'], 'slug' => $item['slug']]) }}">
                {{ $item['title'] }}
            </a>
        </h4>
        <div class="desc" style="text-align: justify;">
            {!! $item['description'] !!}
        </div>
    </div>
@endforeach