<div class="boxes-side design-popular">
    <div class="title-side">
        <span id="ctl00_ContentPlaceHolder1_Sidebar_designpopular_lbTitleCat2">Kiến thức kiến
            trúc</span>
    </div>
    <div class="contain clearfm">
        <ul class="list-article-side">
            @foreach ($knowledges as $item)
                <li class="item">
                    <div class="postImg">
                        <a href="{{ route('article.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                            <img src="{{ asset('images/articles/x400/'.$item['thumbnail']) }}"
                                alt="{{ $item['title'] }}" />
                        </a>
                    </div>
                    <div class="text">
                        <h4>
                            <a href="{{ route('article.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                                {{ $item['title'] }}
                            </a>
                        </h4>
                        <div class="info transparent">
                            <span class="icon views">
                                {{ $item['view'] }}
                            </span>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</div>