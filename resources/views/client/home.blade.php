@extends('client.layouts.template')

@section('title', $contact->title)
@section('key-description', $contact->desc)
@section('keywords', $contact->keywords)

@section('content')
<article>
    <div class="boxes menu-default">
        <div class="container">
            <div class="title-cat">
                <span class="clearfm">
                    <a href="{{ route('project.category',['slug' => 'biet-thu', 'id' => 0]) }}">
                        {{ __('Biệt thự') }}
                    </a>
                </span>
            </div>
            <div class="SlideLeftOject">
                <div class="contain overHide clearfm">
                    <ul class="overHide feature-home">
                        @include('client.home.project',['id' => 0])
                    </ul>
                </div>
                <div class="next"></div>
                <div class="prev"></div>
                <a class="all"
                href="{{ route('project.category',['slug' => 'biet-thu', 'id' => 0]) }}">
                    Xem tất cả
                </a>
            </div>
        </div>
    </div>
    <div class="boxes menu-default">
        <div class="container">
            <div class="title-cat">
                <span class="clearfm">
                    <a href="{{ route('project.category',['slug' => 'biet-thu-tret', 'id' => 1]) }}">
                        {{ __('Biệt thự trệt') }}
                    </a>
                </span>
            </div>
            <div class="SlideLeftOject">
                <div class="contain overHide clearfm">
                    <ul class="overHide feature-home">
                        @include('client.home.project',['id' => 1])
                    </ul>
                </div>
                <div class="next"></div>
                <div class="prev"></div>
                <a class="all"
                href="{{ route('project.category',['slug' => 'biet-thu-tret', 'id' => 1]) }}">
                    Xem tất cả
                </a>
            </div>
        </div>
    </div>
    <div class="boxes menu-default">
        <div class="container">
            <div class="title-cat">
                <span class="clearfm">
                    <a href="{{ route('project.category',['slug' => 'nha-pho', 'id' => 2]) }}">
                        {{ __('Nhà phố') }}
                    </a>
                </span>
            </div>
            <div class="SlideLeftOject">
                <div class="contain overHide clearfm">
                    <ul class="overHide feature-home">
                        @include('client.home.project',['id' => 2])
                    </ul>
                </div>
                <div class="next"></div>
                <div class="prev"></div>
                <a class="all"
                href="{{ route('project.category',['slug' => 'nha-pho', 'id' => 2]) }}">
                    Xem tất cả
                </a>
            </div>
        </div>
    </div>
    <div class="boxes banner-home">
        <div class="container">
            <div class="img">
                <a href="home.html">
                    <img src="{{ asset("client/assets/uploads/images/Gif.gif") }}" alt="Banner trang chủ" />
                </a>
            </div>
        </div>
    </div>
    <div class="boxes menu-default">
        <div class="container">
            <div class="title-cat">
                <span class="clearfm">
                    <a href="{{ route('project.category',['slug' => 'nha-cap-4', 'id' => 3]) }}">
                        {{ __('Nhà cấp 4') }}
                    </a>
                </span>
            </div>
            <div class="SlideLeftOject">
                <div class="contain overHide clearfm">
                    <ul class="overHide feature-home">
                        @include('client.home.project',['id' => 3])
                    </ul>
                </div>
                <div class="next"></div>
                <div class="prev"></div>
                <a class="all"
                href="{{ route('project.category',['slug' => 'nha-cap-4', 'id' => 3]) }}">
                    Xem tất cả
                </a>
            </div>
        </div>
    </div>
    <div class="boxes menu-default">
        <div class="container">
            <div class="title-cat">
                <span class="clearfm">
                    <a href="{{ route('project.category',['slug' => 'cong-trinh-thi-cong', 'id' => 4]) }}">
                        {{ __('Công trình thi công') }}
                    </a>
                </span>
            </div>
            <div class="SlideLeftOject">
                <div class="contain overHide clearfm">
                    <ul class="overHide feature-home">
                        @include('client.home.project',['id' => 4])
                    </ul>
                </div>
                <div class="next"></div>
                <div class="prev"></div>
                <a class="all"
                href="{{ route('project.category',['slug' => 'cong-trinh-thi-cong', 'id' => 4]) }}">
                    Xem tất cả
                </a>
            </div>
        </div>
    </div>
    <div class="boxes menu-default">
        <div class="container">
            <div class="title-cat">
                <span class="clearfm">
                    <a href="{{ route('project.category',['slug' => 'noi-that', 'id' => 5]) }}">
                        {{ __('Nội thất') }}
                    </a>
                </span>
            </div>
            <div class="SlideLeftOject">
                <div class="contain overHide clearfm">
                    <ul class="overHide feature-home">
                        @include('client.home.project',['id' => 5])
                    </ul>
                </div>
                <div class="next"></div>
                <div class="prev"></div>
                <a class="all"
                href="{{ route('project.category',['slug' => 'noi-that', 'id' => 5]) }}">
                    Xem tất cả
                </a>
            </div>
        </div>
    </div>
    <section class="mtop40">
        <div class="container">
            <div class="boxes">
                <div class="title-cat clearfm">
                    <span id="ctl00_ContentPlaceHolder1_lbDesignArticle">Kiến thức</span>
                    <a id="ctl00_ContentPlaceHolder1_hplDesignArticle" class="link"
                        href="{{ route('article.category',['slug' => 'kien-thuc-kien-truc', 'id' => 0]) }}">
                        Xem tất cả
                    </a>
                </div>
                <div class="contain clearfm">
                    @include('client.knowledge.left',['leftKnow' => $leftKnow])
                    @include('client.knowledge.right',['rightKnow' => $rightKnow])
                </div>
            </div>
        </div>
    </section>
</article>

<div class="videohome">
    <div class="container overHide clearfm pdingm">
        <div class="td-product">
            <hgroup class="title-default">
                <h2>Video dự án</h2>
                <h6></h6>
                <h5>{!! $contact->desc_video_projects !!}</h5>
            </hgroup>
        </div>
        <ul class="ulvp">
            @foreach($links as $item)
            <li class="livp">
                <div class="video">
                    <a data-fancybox="youtube" class="youtube" href="{{ $item['link'] }}">
                        @php
                            preg_match('/(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/\s]{11})/', 
                                $item['link'], $youtube_match);
                        @endphp

                        <iframe src="https://www.youtube.com/embed/{{ $youtube_match[1] }}" 
                            frameborder="0" allowfullscreen></iframe>
                    </a>
                </div>
                <h4>
                    <span class="line2">{{ $item['title'] }}</span>
                </h4>
            </li>
            @endforeach
        </ul>
    </div>
</div>

<div class="tuvan-default">
    <div class="container overHide clearfm pdingm">
        <hgroup class="title-tuvan">
            <h2>Hãy gọi ngay chúng tôi</h2>
            <h5>Để được tư vấn miễn phí</h5>
        </hgroup>
        <ul class="ultv">
            <li>
                <a id="ctl00_ContentPlaceHolder1_hplHotline" href="tel:0903%20975%20505">{{ $contact['phone_contact'] }}</a>
            </li>
            <li>
                <a id="ctl00_ContentPlaceHolder1_hplLienhe"
                    href="mailto:kts.dvthuong@gmail.com">{{ $contact['email'] }}</a>
            </li>
        </ul>
    </div>
    <div class="container clearfm flipInY bntuvan">
        <a href="#">
            <img src="{{ asset("client/assets/uploads/images/bn-vinh-hung.jpg") }}" alt="Banner trang chủ" />
        </a>
    </div>
</div>
<div class="about-default">
    <div class="container overHide clearfm pdingm">
        <div class="text-about">
            <hgroup class="title-about">
                <h2>Về chúng tôi</h2>
                <h5>{{ $contact->name_company }}</h5>
                <h6></h6>
            </hgroup>
            <div class="ttgt">
                {!! $contact->desc_info !!}
            </div>
            @php
                $introduce = App\Models\Article::where('type_id','=',7)->get('title');
            @endphp
            <div class="linkgt">
                <a href="{{ route('introduce.detail',['slug' => count($introduce) > 0 ? $introduce[0]->title : 'gioi-thieu', 'id' => 1]) }}">{{ __('Giới thiệu') }} <i class="fal fa-angle-right"></i></a>
            </div>
        </div>
        <div class="picture-about">
            <img class="imggt imggt1" src="{{ asset("client/assets/images/imggt1.png") }}" />
            <img class="imggt imggt2" src="{{ asset("client/assets/images/imggt2.png") }}" />
        </div>
    </div>
</div>
@endsection