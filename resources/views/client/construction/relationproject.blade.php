<div class="boxes">
    <div class="title-cat">
        <span>{{ __('Dự án liên quan') }}
        </span>
    </div>
    <div class="contain border clearfm">
        <ul class="list-same-post">  
            @foreach ($relationProject as $item)
                <li class="overHide item">
                    <div class="postImg">
                        <a href="{{ route('project.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                            <img src="{{ asset('images/projects/'.(count($item['image']) > 0 ? $item['image'][0]['image_src'] : '')) }}" alt="{{ $item['title'] }}" />
                        </a>
                    </div>
                    <div class="text">
                        <h4>
                            <a
                                href="{{ route('project.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                                <span>{{ $item['title'] }}</span>
                            </a>
                        </h4>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</div>