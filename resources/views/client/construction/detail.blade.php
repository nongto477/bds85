@if (isset($project) && !is_null($project))
    <div class="box-side boxside">
        <div class="title-side">
            <span>Thông tin dự án</span>
        </div>
        <div class="contain clearfm">
            <ul class="ulside load-cont">
                <li>Tên công trình:
                    <span id="ctl00_ContentPlaceHolder1_lbTitlePost">{{ $project['title'] }}</span>
                </li>
                <li>Địa chỉ:
                    <span id="ctl00_ContentPlaceHolder1_lbDiaChi">{{ $project['address'] }}</span>
                </li>
                <li>Diện tích:
                    <span id="ctl00_ContentPlaceHolder1_lbDienTich">{{ $project['area'] }} m2</span>
                </li>
                <li>Số tầng:
                    <span id="ctl00_ContentPlaceHolder1_lbSoTang">{{ $project['number_floor'] }}</span>
                </li>
                <li>Chi phí xây dựng:
                    <span id="ctl00_ContentPlaceHolder1_lbChiPhi">@money($project['fee']) ₫</span>
                </li>
                <li>Dịch vụ:
                    <span id="ctl00_ContentPlaceHolder1_lbDichVu">{{ $project['service'] }}</span>
                </li>
                <li>Năm hoàn thành:
                    <span id="ctl00_ContentPlaceHolder1_lbNam">{{ $project['year_complete'] }}</span>
                </li>
            </ul>
        </div>
    </div>
@endif