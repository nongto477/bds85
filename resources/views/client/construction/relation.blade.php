<div class="boxes">
    <div class="title-cat">
        <span>{{ __('Bài liên quan') }}
        </span>
    </div>
    <div class="contain border clearfm">
        <ul class="list-same-post">  
            @foreach ($relationArticle as $item)
                <li class="overHide item">
                    <div class="postImg">
                        <a href="{{ route('article.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                            <img src="{{ asset('images/articles/x400/'.$item['thumbnail']) }}"
                                alt="{{ $item['title'] }}" />
                        </a>
                    </div>
                    <div class="text">
                        <h4>
                            <a
                                href="{{ route('article.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                                <span>{{ $item['title'] }}</span>
                            </a>
                        </h4>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</div>