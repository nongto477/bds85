<div class="boxes-side design-side">
    <div class="title-side">
        <span>{{ __('Công trình thi công') }}</span>
    </div>
    <div class="contain clearfm">
        <ul class="list-article-side">
            @foreach ($contrs as $item)
                <li class="item">
                    <div class="postImg">
                        <a href="{{ route('project.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                            <img src="{{ asset('images/projects/'.(count($item['image']) > 0 ? $item['image'][0]['image_src'] : '')) }}" alt="{{ $item['title'] }}" />
                        </a>
                    </div>
                    <div class="text">
                        <h4>
                            <a href="{{ route('project.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                                {{ $item['title'] }}
                            </a>
                        </h4>
                        <div class="info transparent">
                            <span class="icon views">
                                {{ $item['view'] }}
                            </span>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</div>