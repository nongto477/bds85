@php
    $bt = 1;
    $btt = 1;
    $np = 1;
    $np4 = 1;
    $cttc = 1;
    $nt = 1;
@endphp
@foreach ($projects as $item)
    @if ($item['type_id'] == $id)
        {{-- Biệt thự --}}
        @if ($id == 0 && $bt < 9)
            @php $bt++; @endphp
            <li class="item">
                <a
                    href="{{ route('project.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                    <div class="content">
                        <div class="postImg">
                            <img src="{{ asset('images/projects/x400/'.(count($item['image']) > 0 ? $item['image'][0]['image_src'] : '')) }}" alt="{{ $item['title'] }}" />
                        </div>
                        <span class="views">{{ $item['view'] }}</span>
                    </div>
                </a>
                <h4>
                    <a
                        href="{{ route('project.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                        <span>{{ $item['title'] }}</span>
                    </a>
                </h4>
            </li>
        {{-- Biệt thự trệt --}}
        @elseif ($id == 1 && $btt < 9)
            @php $btt++; @endphp
            <li class="item">
                <a
                    href="{{ route('project.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                    <div class="content">
                        <div class="postImg">
                            <img src="{{ asset('images/projects/x400/'.(count($item['image']) > 0 ? $item['image'][0]['image_src'] : '')) }}" alt="{{ $item['title'] }}" />
                        </div>
                        <span class="views">{{ $item['view'] }}</span>
                    </div>
                </a>
                <h4>
                    <a
                        href="{{ route('project.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                        <span>{{ $item['title'] }}</span>
                    </a>
                </h4>
            </li>
        {{-- Nhà phố --}}
        @elseif ($id == 2 && $np < 9)
            @php $np++; @endphp
            <li class="item">
                <a
                    href="{{ route('project.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                    <div class="content">
                        <div class="postImg">
                            <img src="{{ asset('images/projects/x400/'.(count($item['image']) > 0 ? $item['image'][0]['image_src'] : '')) }}" alt="{{ $item['title'] }}" />
                        </div>
                        <span class="views">{{ $item['view'] }}</span>
                    </div>
                </a>
                <h4>
                    <a
                        href="{{ route('project.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                        <span>{{ $item['title'] }}</span>
                    </a>
                </h4>
            </li>
        {{-- Nhà cấp 4 --}}
        @elseif ($id == 3 && $np4 < 9)
            @php $np4++; @endphp
            <li class="item">
                <a
                    href="{{ route('project.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                    <div class="content">
                        <div class="postImg">
                            <img src="{{ asset('images/projects/x400/'.(count($item['image']) > 0 ? $item['image'][0]['image_src'] : '')) }}" alt="{{ $item['title'] }}" />
                        </div>
                        <span class="views">{{ $item['view'] }}</span>
                    </div>
                </a>
                <h4>
                    <a
                        href="{{ route('project.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                        <span>{{ $item['title'] }}</span>
                    </a>
                </h4>
            </li>
        {{-- Công trình thi công --}}
        @elseif ($id == 4 && $cttc < 9)
            @php $cttc++; @endphp
            <li class="item">
                <a
                    href="{{ route('project.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                    <div class="content">
                        <div class="postImg">
                            <img src="{{ asset('images/projects/x400/'.(count($item['image']) > 0 ? $item['image'][0]['image_src'] : '')) }}" alt="{{ $item['title'] }}" />
                        </div>
                        <span class="views">{{ $item['view'] }}</span>
                    </div>
                </a>
                <h4>
                    <a
                        href="{{ route('project.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                        <span>{{ $item['title'] }}</span>
                    </a>
                </h4>
            </li>
        {{-- Nội thất --}}
        @elseif ($id == 5 && $nt < 9)
            @php $nt++; @endphp
            <li class="item">
                <a
                    href="{{ route('project.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                    <div class="content">
                        <div class="postImg">
                            <img src="{{ asset('images/projects/x400/'.(count($item['image']) > 0 ? $item['image'][0]['image_src'] : '')) }}" alt="{{ $item['title'] }}" />
                        </div>
                        <span class="views">{{ $item['view'] }}</span>
                    </div>
                </a>
                <h4>
                    <a
                        href="{{ route('project.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                        <span>{{ $item['title'] }}</span>
                    </a>
                </h4>
            </li>  
        @endif
    @endif
@endforeach