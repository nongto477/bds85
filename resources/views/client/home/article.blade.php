@foreach ($articles as $item)
    @if ($item['type_id'] == $id)
        <li class="item">
            <a
                href="{{ route('article.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                <div class="content">
                    <div class="postImg">
                        <img src="{{ asset('images/articles/x400/'.$item['thumbnail']) }}"
                            alt="{{ $item['title'] }}" />
                    </div>
                    <span class="views">{{ $item['view'] }}</span>
                </div>
            </a>
            <h4>
                <a
                    href="{{ route('article.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                    <span>{{ $item['title'] }}</span>
                </a>
            </h4>
        </li>
    @endif
@endforeach