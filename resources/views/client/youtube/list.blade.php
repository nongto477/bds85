@extends('client.layouts.template')

@section('title','Video')

@section('content')
<article id="Wrapper" class="Section">
    <div class="container">
        <section>
            <div class="boxes">
                <div class="title-cat">
                    <span id="ctl00_ContentPlaceHolder1_lbTitleCat">{{ __('Video') }}</span>
                </div>
                <div class="container overHide clearfm pdingm">
                    <ul class="ulvp">
                        @foreach($videos as $item)
                        <li class="livp">
                            <div class="video">
                                <a data-fancybox="youtube" class="youtube" href="{{ $item['link'] }}">
                                    @php
                                        preg_match('/(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/\s]{11})/', 
                                            $item['link'], $youtube_match);
                                    @endphp
            
                                    <iframe src="https://www.youtube.com/embed/{{ $youtube_match[1] }}" 
                                        frameborder="0" allowfullscreen></iframe>
                                </a>
                            </div>
                            <h4>
                                <span class="line2">{{ $item['title'] }}</span>
                            </h4>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </section>
        {{-- <aside class="col-side fixed">
            @include('client.construction.list',['contrs' => $contrs])
            @include('client.knowledge.list',['knowledges' => $knowledges])
        </aside> --}}
    </div>
</article>
@endsection