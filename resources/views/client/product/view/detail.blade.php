@extends('client.layouts.template')

@section('title', $title)

@section('title', $product['seo_title'])

@section('keywords', $product['keyword'])

@section('key-description', $product['key_description'])

@section('content')
<article id="Wrapper" class="Section">
    <div class="container">
        <section class="col-section">
            <div class="boxes">
                <div class="contain border clearfm">
                    <div class="clearfm title_duan">
                        <h5 style="text-align: left">
                            <span>{{ $product['seo_title'] }}</span>
                        </h5>
                    </div>
                    <div style="display: flex; align-items: flex-start;">
                        <section class="slider" style="display: inline-block; width: 40%">
                            <div class="flexslider">
                                <ul class="slides">
                                    @if (count($product["image"]) > 0)
                                        @foreach ($product["image"] as $item)
                                            <li data-thumb="{{ asset('images/products/x400/'.$item['image_src']) }}">
                                                <img src="{{ asset('images/products/x400/'.$item['image_src']) }}" />
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </section>
                        <script type='text/javascript' src='{{ asset("/client/assets/scripts/FlexSlider2/js/jquery.flexslider.js") }}'></script>
                        <script type="text/javascript">
                            $(window).load(function () {
                                $('.flexslider').flexslider({
                                    animation: "slide",
                                    controlNav: "thumbnails",
                                });
                            });
                        </script>

                        <div class="product-center" style="display: inline-block; width: 50%">
                            <p class="price current-product-price">
                                <strong>
                                    @money($product["price"]) ₫
                                </strong><br>
                                <br>
                                GIá tốt liên hệ {{$contact['phone_sale']}}
                                <br>
                                <br>
                            </p>
                            <p class="freeship">
                                <i class="icon-freeship-truck"></i>
                                <span style="display: block; padding: .5rem">
                                    Địa chỉ mua hàng: {{ $contact['address'] }}
                                </span>
                            </p>
                        </div>
                    </div>
                    <div class="detail">
                        <h2>{{ __('Mô tả sản phẩm') }}</h2>
                        <h2 class="description">{{ $product['description'] }}</h2>
                        <br>
                        {!! $product['content'] !!}
                    </div>
                </div>
            </div>
        </section>
        <aside class="col-side fixed">
            @include('client.construction.list', ['contrs' => $contrs])
            @include('client.knowledge.list', ['knowledges' => $knowledges])
        </aside>
    </div>
</article>
@endsection
