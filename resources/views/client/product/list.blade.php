@extends('client.layouts.template')

@section('title','Sản phẩm')

@section('content')
<article id="Wrapper" class="Section">
    <div class="container">
        <section class="col-section">
            <div class="boxes">
                <div class="title-cat">
                    <span id="ctl00_ContentPlaceHolder1_lbTitleCat">Tất cả sản phẩm</span>
                </div>
                <div class="contain border clearfm">
                    @if (count($products) > 0)
                        <ul class="overHide feature-home">
                            @foreach($products as $item)
                            <li class="item item-category">
                                <a
                                    href="{{ route('product.detail', ['id' => $item->id, 'slug' => $item->slug]) }}">
                                    <div class="content">
                                        <div class="postImg">
                                            <img src="{{ asset('images/products/'.$item['image'][0]['image_src']) }}"
                                                alt="{{ $item->title }}" />
                                        </div>
                                        <span class="views">{{ $item->view }}</span>
                                    </div>
                                </a>
                                <h4>
                                    <a
                                        href="{{ route('product.detail', ['id' => $item->id, 'slug' => $item->slug]) }}">
                                        <span>{{ $item->title }}</span>
                                    </a>
                                </h4>
                            </li>
                            @endforeach
                        </ul>
                    @else
                        <div style="color:white; font-size:1.2rem;">{{ __('Nội dung chúng tôi sẽ cập nhật sau') }}</div>
                    @endif
                    {{ $products->links() }}
                </div>
            </div>
        </section>
        <aside class="col-side fixed">
            @include('client.construction.list')
            @include('client.knowledge.list')
        </aside>
    </div>
</article>
@endsection