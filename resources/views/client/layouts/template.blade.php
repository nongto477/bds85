<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href='{{ asset("images/favicons/$contact->favicon") }}' type="image/png" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset("client/assets/css/public8e5e.css?v=15") }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset("client/assets/css/style80ba.css?v=23") }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset("client/assets/css/style.css") }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset("client/assets/css/categorys7b30.css") }}" />
    <link rel="stylesheet" href="{{ asset("client/assets/scripts/source/jquery.fancybox8cbb.css?v=2.1.5") }}" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ asset("client/assets/scripts/source/helpers/jquery.fancybox-buttons3447.css?v=1.0.5") }}" />
    <link rel="stylesheet" href="{{ asset("client/assets/scripts/back-to-top/css/style5e1f.css?v=2") }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset("client/assets/scripts/diapo-v101/diapo.css") }}" type="text/css" media="all" />
    <link rel="stylesheet" href="{{ asset("client/assets/scripts/FlexSlider2/css/flexslider.css") }}" type="text/css" media="screen" />
    <link href="{{ asset("client/assets/scripts/fontawesome/css/all.css") }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset("client/assets/scripts/malihu-custom-scrollbar/jquery.mCustomScrollbar.css") }}" />
    <meta name="description" content="@yield('key-description')" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="vi_VN">
    <meta name="keywords" content="@yield('keywords')" />
    <title>@yield('title')</title>
</head>

<body>
    <div class="overAll">
        <header>
            <div class="top-content">
                <div class="container overHide clearfm">
                    <div id="show-menu">Menu</div>
                    <div class="left hotline">{{ $contact['name_company'] }}</div>
                    <div class="right">
                        <div class="menu-top">
                            @php
                                $introduce = App\Models\Article::where('type_id','=',7)->get('title');
                            @endphp
                            <a href="{{ route('introduce.detail',['slug' => count($introduce) > 0 ? $introduce[0]->title : 'gioi-thieu', 'id' => 1]) }}">{{ __('Giới thiệu') }}</a>
                            <a href="{{ route('article.category',['slug' => 'kien-thuc-kien-truc', 'id' => 0]) }}">{{ __('Kiến thức') }}</a>
                            <a href="{{ route('youtube') }}">{{ __('Video') }}</a>
                            <div style="display: inline-block">
                                <form class="search" method="GET" enctype="multipart/form-data" 
                                    action="{{ route('search') }}">
                                    <input type="text" class="searchTerm" name="q" placeholder="Tìm kiếm...">
                                    <button type="submit" class="searchButton">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="container overHide clearfm">
                    <div class="overHide">
                        <h1 class="logo img">
                            <a href="{{ route('home') }}">
                                <img src="{{ asset("client/assets/images/logoDE.png") }}" alt="Hoai DeDesign - Thiết kế nhà phố đẹp Phan Thiết" />
                            </a>
                        </h1>
                        <div class="slogannone">{{ $contact['name_company'] }}</div>
                    </div>
                    <nav>
                        <ul class="menuMain">
                            <li class="home"><a href="{{ route('home') }}">{{ __('Trang chủ') }}</a></li>
                            <li><a href='{{ route('project.category', ['slug' => 'biet-thu', 'id' => 0]) }}'>{{ __('Biệt thự') }}</a></li>
                            <li><a href='{{ route('project.category', ['slug' => 'biet-thu-tret', 'id' => 1]) }}'>{{ __('Biệt thự trệt') }}</a></li>
                            <li><a href='{{ route('project.category', ['slug' => 'nha-pho', 'id' => 2]) }}'>{{ __('Nhà phố') }}</a></li>
                            <li><a href='{{ route('project.category', ['slug' => 'nha-cap-4', 'id' => 3]) }}'>{{ __('Nhà cấp 4') }}</a></li>
                            <li><a href='{{ route('project.category', ['slug' => 'cong-trinh-thi-cong', 'id' => 4]) }}'>{{ __('Công trình thi công') }}</a></li>
                            <li>
                                <div class="dropdown">
                                    <a class="dropbtn" href='{{ route('article.category',['slug' => 'bang-gia', 'id' => 3]) }}'>{{ __('Bảng giá') }}</a>
                                    <ul class="dropdown-content">
                                        @foreach($banggia as $item)
                                            <li><a href="{{ route('article.detail', ['slug' => $item->slug, 'id' => $item->id]) }}">{{ __($item->title) }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                            <li><a href='{{ route('project.category',['slug' => 'noi-that', 'id' => 5]) }}'>{{ __('Nội thất') }}</a></li>
                            <li>
                                <div class="dropdown">
                                    <a class="dropbtn" href='{{ route("client.products") }}'>{{ __('Sản phẩm') }}</a>
                                    <ul class="dropdown-content">
                                        @foreach($categories as $item)
                                            <li><a href="{{ route('product.category', ['id' => $item['id']]) }}">{{ __($item['name']) }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                            <li><a href='{{ route('article.category',['slug' => 'tin-tuc', 'id' => 2]) }}'>{{ __('Tin tức') }}</a></li>
                        </ul>
                    </nav>
                    <div class="close"></div>
                </div>
            </div>
            <div id="slideshow" style='max-height: 50vh'>
                <div class="pix_diapo">
                    @foreach ($banners as $item)
                        <div data-thumb="{{ asset('images/banners/x1200/'.$item['thumbnail']) }}">
                            <img src="{{ asset('images/banners/x1200/'.$item['thumbnail']) }}"/>
                        </div>
                    @endforeach
                </div>
            </div>
        </header>
        @yield('content')
        <footer>
            <div class="content">
                <div class="container overHide clearfm">
                    <div class="infocompay-footer left">
                        <h1>{{ $contact['name_company'] }}</h1>
                        <p><i class="fas fa-map-marker-alt"></i> Địa chỉ: {{ $contact->address }}</p>
                        <p><i class="fas fa-phone-volume"></i> Điện thoại: {{ $contact->phone }}</p>
                        <p><i class="fas fa-envelope"></i> Email: {{ $contact->email }}</p>
                    </div>

                    <ul class="menu-footer right">
                        <li>
                            <a href="{{ route('project.category',['slug' => 'biet-thu', 'id' => 0]) }}">
                                Biệt thự
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('project.category',['slug' => 'biet-thu-tret', 'id' => 1]) }}">
                                Biệt thự trệt
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('project.category',['slug' => 'nha-pho', 'id' => 2]) }}">
                                Nhà phố
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('project.category',['slug' => 'nha-cap-4', 'id' => 3]) }}">
                                Nhà cấp 4
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('project.category',['slug' => 'cong-trinh-thi-cong', 'id' => 4]) }}">
                                Công trình thi công
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('project.category',['slug' => 'noi-that', 'id' => 5]) }}">
                                Nội thất
                            </a>
                        </li>
                        <li>
                            <a href='{{ route('article.category',['slug' => 'bang-gia', 'id' => 3]) }}'>
                                {{ __('Bảng giá') }}
                            </a>
                        </li>
                        <li><a href='{{ route("client.products") }}'>{{ __('Sản phẩm') }}</a></li>
                    </ul>
                </div>
            </div>
            <div class="foot">
                <div class="container overHide clearfm">
                    <div class="left">
                        <p>&copy; Bản quyền 2021 thuộc về {{ $contact['name_company'] }}. Thiết kế bởi <b>TemDepWeb</b></p>
                    </div>
                </div>
            </div>
        </footer>

        <div class="icon_menu"></div>
        <a href="#0" class="cd-top" style="margin-right:3rem;"><i class="fab fa-angle-up" aria-hidden="true"></i></a>
        <div class="hotline-fixed clearfm">
            <div class="hotline-icon"><i class="fab fa-phone" aria-hidden="true"></i></div>
            <div class="hotline-number"><a href="tel:{{ $contact['phone_contact'] }}" class="sdt sdt1">
                {{ $contact['name_contact'] }}: {{ $contact['phone_contact'] }}</a>
                <div class="close"></div>
            </div>
        </div>
    </div>
    <!-- Messenger Plugin chat Code -->
    <div id="fb-root"></div>

    <!-- Your Plugin chat code -->
    <div id="fb-customer-chat" class="fb-customerchat">
    </div>
    <div class="mzalo">
        <a href="http://zalo.me/{{ $contact['zalo'] }}" target="_blank"
            style="display:block;width:100%;height:100%;position:relative;">
            <img src="{{ asset("client/assets/images/mzalo.png") }}" style="position: absolute;left: 0;bottom: 0;width: 50px;height: 50px;" />
        </a>
    </div>
    <div class="mmap">
        <a href="{{ $contact['google_map'] }}"
            target="_blank" style="display:block;width:100%;height:100%;position:relative;">
            <img src="{{ asset("client/assets/images/icon-maps2.png") }}" style="position: absolute;left: 0;bottom: 0;width: 50px;" />
        </a>
    </div>
    <div class="yt">
        <a href="{{ $contact['youtube'] }}" target="_blank"
            style="display:block;width:100%;height:100%;position:relative;">
            <img src="{{ asset("client/assets/images/icon-youtube.png") }}" style="display:block; width: 50px;" />
        </a>
    </div>
    <!-- <div class="pi">
        <a href="https://www.pinterest.com/ctykientrucdeimages/" target="_blank"
            style="display:block;width:100%;height:100%;position:relative;">
            <img src="{{ asset("client/assets/images/p.png") }}" style="display:block; width: 50px;" />
        </a>
    </div> -->

    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <!--[if !IE]><!-->
    <script type='text/javascript' src='{{ asset("client/assets/scripts/diapo-v101/scripts/jquery.mobile-1.0rc2.customized.min.js") }}'></script>
    <!--<![endif]-->
    <script type='text/javascript' src='{{ asset("client/assets/scripts/diapo-v101/scripts/jquery.easing.1.3.js") }}'></script>
    <script type='text/javascript' src='{{ asset("client/assets/scripts/diapo-v101/scripts/jquery.hoverIntent.minified.js") }}'></script>
    <script type='text/javascript' src='{{ asset("client/assets/scripts/diapo-v101/scripts/diapo.js") }}'></script>
    <script type="text/javascript" src="{{ asset("client/assets/scripts/source/jquery.fancybox.pack8cbb.js?v=2.1.5") }}"></script>
    <script type="text/javascript" src="{{ asset("client/assets/scripts/source/helpers/jquery.fancybox-buttons3447.js?v=1.0.5") }}"></script>
    <script src="{{ asset("client/assets/scripts/malihu-custom-scrollbar/jquery.mCustomScrollbar.concat.min.js") }}"></script>
    <script src="{{ asset("client/assets/scripts/back-to-top/js/main.js") }}"></script>
    <script src="{{ asset("client/assets/scripts/ScrollToFixed/jquery-scrolltofixed-min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("client/assets/scripts/ScrollToFixed/jquery.sticky-kit.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("client/assets/scripts/js/script.js") }}"></script>
</body>

</html>