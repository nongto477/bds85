@extends('client.layouts.template')

@if (isset($project) && !is_null($project))
    @section('title',$project['seo_title'])

    @section('keywords',$project['keyword'])

    @section('key-description',$project['key_description'])

    @section('content')
    <article id="Wrapper" class="Section">
        <div class="container">
            <section class="col-section">
                <div class="boxes">
                    <div class="title-cat">
                        <span>
                            @php
                                switch ($project['type_id']) {
                                    case "0":
                                        echo __('Biệt thự');
                                        break;
                                    case "1":
                                        echo __('Biệt thự trệt');
                                        break;
                                    case "2":
                                        echo __('Nhà phố');
                                        break;
                                    case "3":
                                        echo __('Nhà cấp 4');
                                        break;  
                                    case "4":
                                        echo __('Công trình thi công');
                                        break; 
                                    case "5":
                                        echo __('Nội thất');
                                        break; 
                                }
                            @endphp
                        </span>
                    </div>
                    <h1 class="title-article">{{ $project['seo_title'] }}</h1>
                    <div id="fb-root"></div>
                    <script async defer 
                        crossorigin="anonymous" 
                        src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v11.0&appId=358437268550028&autoLogAppEvents=1" 
                        nonce="PNakU7hd">
                    </script>
                    <div class="info">
                        <div class="share">
                            <div class="fb-like" data-href="{{ route('project.detail',['id' => $project['id'], 'slug' => $project['slug']]) }}" data-width="" data-layout="button" data-action="like" data-size="small" data-share="true"></div>
                        </div>
                        <span id="ctl00_ContentPlaceHolder1_lbDate" class="icon date">{{ date('d-m-Y',strtotime($project['created_at'])) }}</span>
                        <span id="ctl00_ContentPlaceHolder1_lbCount" class="icon views">{{ $project['view'] }}</span>
                    </div>
                    <div class="detail">
                        <h2 class="description">{{ $project['description'] }}</h2>
                        {!! $project['content'] !!}
                        <br>
                        @if (count($project["image"]) > 0)
                            <section class="slider" style="display: inline-block; width: 100%">
                                <div class="flexslider">
                                    <ul class="slides">
                                        @foreach ($project["image"] as $item)
                                            <li data-thumb="{{ asset('images/projects/'.$item['image_src']) }}">
                                                <img src="{{ asset('images/projects/'.$item['image_src']) }}"/>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </section>
                            <script type='text/javascript' src='{{ asset("/client/assets/scripts/FlexSlider2/js/jquery.flexslider.js") }}'></script>
                            <script type="text/javascript">
                                $(window).load(function () {
                                    $('.flexslider').flexslider({
                                        animation: "slide",
                                        controlNav: "thumbnails"
                                    });
                                });
                            </script>
                        @endif
                    </div>
                    @include('client.construction.relationproject',['relationProject' => $relationProject])
            </section>
            <aside class="col-side fixed">
                @include('client.construction.detail',['project' => $project])
                @include('client.construction.list',['contrs' => $contrs])
                @include('client.knowledge.list',['knowledges' => $knowledges])
            </aside>
        </div>
    </article>
    @endsection
@else
    @section('title','')

    @section('keywords','')

    @section('key-description','')

    @section('content')
    <article id="Wrapper" class="Section">
        <div class="container">
            <section class="col-section">
                <div class="boxes">
                    <div class="title-cat">
                        <span>
                           {{ __('Giới thiệu') }}
                        </span>
                    </div><br />
                    <div style="color:white; font-size:1.2rem;">{{ __('Nội dung chúng tôi sẽ cập nhật sau') }}</div>
            </section>
            <aside class="col-side fixed">
                @include('client.construction.list',['contrs' => $contrs])
                @include('client.knowledge.list',['knowledges' => $knowledges])
            </aside>
        </div>
    </article>
    @endsection
@endif