@extends('client.layouts.template')

@section('title', $title)

@section('content')
<article id="Wrapper" class="Section">
    <div class="container">
        <section class="col-section">
            <div class="boxes">
                <div class="title-cat">
                    <span>{{ $title }}</span>
                </div>
                <div class="contain border clearfm">
                    <div class="title-cat" style="border: none; margin-bottom: 1.5rem; font-size: 1.2rem">
                        {{ __('Sản phẩm') }}
                    </div>
                    @if (count($products) > 0)
                        <ul class="overHide feature-home">
                            @foreach ($products as $item)
                                <li class="item item-category">
                                    <a
                                        href="{{ route('product.detail', ['id' => $item['id'], 'slug' => $item['slug']]) }}">
                                        <div class="content">
                                            <div class="postImg">
                                                <img src="{{ asset('images/products/'.(count($item['image']) > 0 ? $item['image'][0]['image_src'] : '')) }}" alt="{{ $item['title'] }}" />
                                            </div>
                                            <span class="views">{{ $item['view'] }}</span>
                                        </div>
                                    </a>
                                    <h4>
                                        <a
                                            href="{{ route('product.detail', ['id' => $item['id'],'slug' => $item['slug']]) }}">
                                            <span>{{ $item['title'] }}</span>
                                        </a>
                                    </h4>
                                </li>
                            @endforeach
                        </ul>
                    @else
                        <div style="color:white; font-size:1.2rem; margin-bottom: 1.5rem">{{ __('Không tìm thấy kết quả') }}</div>
                    @endif
                    {{ $products->links() }}

                    <div class="title-cat" style="border: none; margin-top: 1.5rem; margin-bottom: 1.5rem; font-size: 1.2rem">
                        {{ __('Bài viết') }}
                    </div>
                    @if (count($articles) > 0)
                        <ul class="overHide feature-home">
                            @foreach ($articles as $item)
                                <li class="item item-category">
                                    <a
                                        href="{{ route('article.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                                        <div class="content">
                                            <div class="postImg">
                                                <img src="{{ asset('images/articles/x400/'.$item['thumbnail']) }}"
                                                    alt="{{ $item['title'] }}" />
                                            </div>
                                            <span class="views">{{ $item['view'] }}</span>
                                        </div>
                                    </a>
                                    <h4>
                                        <a
                                            href="{{ route('article.detail',['id' => $item['id'],'slug' => $item['slug']]) }}">
                                            <span>{{ $item['title'] }}</span>
                                        </a>
                                    </h4>
                                </li>
                            @endforeach
                        </ul>
                    @else
                        <div style="color:white; font-size:1.2rem; margin-bottom: 1.5rem">{{ __('Không tìm thấy kết quả') }}</div>
                    @endif
                    {{ $articles->links() }}

                    <div class="title-cat" style="border: none; margin-top: 1.5rem; margin-bottom: 1.5rem; font-size: 1.2rem">
                        {{ __('Dự án') }}
                    </div>
                    @if (count($projects) > 0)
                        <ul class="overHide feature-home">
                            @foreach ($projects as $item)
                                <li class="item item-category">
                                    <a href="{{ route('project.detail', ['id' => $item['id'], 'slug' => $item['slug']]) }}">
                                        <div class="content">
                                            <div class="postImg">
                                                <img src="{{ asset('images/projects/'.(count($item['image']) > 0 ? $item['image'][0]['image_src'] : '')) }}" alt="{{ $item['title'] }}" />
                                            </div>
                                            <span class="views">{{ $item['view'] }}</span>
                                        </div>
                                    </a>
                                    <h4>
                                        <a href="{{ route('project.detail', ['id' => $item['id'], 'slug' => $item['slug']]) }}">
                                            <span>{{ $item['title'] }}</span>
                                        </a>
                                    </h4>
                                </li>
                            @endforeach
                        </ul>
                        {{ $projects->links() }}
                    @else
                        <div style="color:white; font-size:1.2rem; margin-bottom: 1.5rem">{{ __('Không tìm thấy kết quả') }}</div>
                    @endif
                </div>
            </div>
        </section>
        <aside class="col-side fixed">
            @include('client.construction.list',['contrs' => $contrs])
            @include('client.knowledge.list',['knowledges' => $knowledges])
        </aside>
    </div>
</article>
@endsection