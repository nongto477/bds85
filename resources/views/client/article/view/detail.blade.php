@extends('client.layouts.template')

@if (isset($article) && !is_null($article))
    @section('title', $article['seo_title'])

    @section('keywords', $article['keyword'])

    @section('key-description', $article['key_description'])

    @section('content')
    <article id="Wrapper" class="Section">
        <div class="container">
            <section class="col-section">
                <div class="boxes">
                    <div class="title-cat">
                        <span>
                            @php
                                switch ($article['type_id']) {
                                    case "0":
                                        echo __('Kiến thức');
                                        break; 
                                    case "2":
                                        echo __('Tin tức');
                                        break;    
                                }
                            @endphp
                        </span>
                    </div>
                    <h1 class="title-article">{{ $article['seo_title'] }}</h1>
                    <div id="fb-root"></div>
                    <script async defer 
                        crossorigin="anonymous" 
                        src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v11.0&appId=358437268550028&autoLogAppEvents=1" 
                        nonce="PNakU7hd">
                    </script>
                    <div class="info">
                        <div class="share">
                            <div class="fb-like" data-href="{{ route('article.detail',['id' => $article['id'], 'slug' => $article['slug']]) }}" data-width="" data-layout="button" data-action="like" data-size="small" data-share="true"></div>
                        </div>
                        <span id="ctl00_ContentPlaceHolder1_lbDate" class="icon date">{{ date('d-m-Y',strtotime($article['created_at'])) }}</span>
                        <span id="ctl00_ContentPlaceHolder1_lbCount" class="icon views">{{ $article['view'] }}</span>
                    </div>
                    <div class="detail">
                        <h2 class="description">{{ $article['description'] }}</h2>
                        {!! $article['content'] !!}
                    </div>
                @if ($article['type_id'] != 1)
                    @include('client.construction.relation',['relationArticle' => $relationArticle])
                @endif
            </section>
            <aside class="col-side fixed">
                @include('client.construction.detail')
                @include('client.construction.list',['contrs' => $contrs])
                @include('client.knowledge.list',['knowledges' => $knowledges])
            </aside>
        </div>
    </article>
    @endsection
@else

    @section('title','')

    @section('keywords','')

    @section('key-description','')

    @section('content')
    <article id="Wrapper" class="Section">
        <div class="container">
            <section class="col-section">
                <div class="boxes">
                    <div class="title-cat">
                        <span>
                           {{ __('Giới thiệu') }}
                        </span>
                    </div><br />
                    <div style="color:white; font-size:1.2rem;">{{ __('Nội dung chúng tôi sẽ cập nhật sau') }}</div>
            </section>
            <aside class="col-side fixed">
                @include('client.construction.list',['contrs' => $contrs])
                @include('client.knowledge.list',['knowledges' => $knowledges])
            </aside>
        </div>
    </article>
    @endsection
@endif