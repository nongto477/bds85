@extends('admin.layouts.template')

@section('title', 'Cập nhật contact')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    @if(Session::has('invalid'))
        <div class="alert alert-danger alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('invalid')}}
        </div>
    @endif
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('success')}}
        </div>
    @endif
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">{{ __('Cập nhật contact') }}</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="container form-text mt-3 mb-5">
            <div class="row">
                <div class="col-sm-12">
                    <!-- Form thêm sản phẩm -->
                    <form method="post" action="{{ route('admin.contact.update') }}" enctype="multipart/form-data">

                        @csrf

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="title">{{ __('Tên trang web') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="title" type="text" name="title" value="{{ $contact['title'] }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="desc">{{ __('Mô tả trang web') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="desc" type="text" name="desc" value="{{ $contact['desc'] }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="keywords">{{ __('Từ khóa trang web') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="keywords" type="text" name="keywords" value="{{ $contact['keywords'] }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="desc_video_projects">{{ __('Mô tả video dự án') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="desc_video_projects" type="text" name="desc_video_projects" value="{{ $contact['desc_video_projects'] }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name_company">{{ __('Tên công ty') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="name_company" type="text" name="name_company" value="{{ $contact['name_company'] }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="desc_info">{{ __('Mô tả công ty') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="desc_info" type="text" name="desc_info" value="{{ $contact['desc_info'] }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="address">{{ __('Địa chỉ công ty') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="address" type="text" name="address" value="{{ $contact['address'] }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="phone">{{ __('Điện thoại') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="phone" type="text" name="phone" value="{{ $contact['phone'] }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="phone_sale">{{ __('Điện thoại nhận đặt hàng') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="phone_sale" type="text" name="phone_sale" value="{{ $contact['phone_sale'] }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="email">{{ __('Email') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="email" type="text" name="email" value="{{ $contact['email'] }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name_contact">{{ __('Tên người liên hệ') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="name_contact" type="text" name="name_contact" value="{{ $contact['name_contact'] }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="phone_contact">{{ __('Số điện thoại liên hệ') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="phone_contact" type="text" name="phone_contact" value="{{ $contact['phone_contact'] }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="youtube">{{ __('Youtube channel') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="youtube" type="text" name="youtube" value="{{ $contact['youtube'] }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="google_map">{{ __('Google map') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="google_map" type="text" name="google_map" value="{{ $contact['google_map'] }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="zalo">{{ __('Zalo') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="zalo" type="text" name="zalo" value="{{ $contact['zalo'] }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="thumbnail">{{ __('Favicon (tối đa 2MB)') }} <span class="text-danger">*</span></label>
                                    <div class="custom-file">
                                        <input type="file" id="thumbnail" name="favicon" accept=".png,.gif,.jpg,.jpeg">
                                    </div>
                                </div>
                                <div class="image-preview mb-4" id="imagePreview">
                                    <img src="{{ asset('images/favicons/'.$contact['favicon']) }}" alt="Image Preview" class="image-preview__image" style="display:block;"/>
                                    <span class="image-preview__default-text" style="display:none">{{ __('Favicon') }}</span>
                                </div>
                                <div id="error" class="text-danger"></div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" name="submit">{{ __('Cập nhật') }}</button>
                            </div>
                        </div>
                    </form>
             </div>
        </div>
    </div>
</div>
@endsection