@extends('admin.layouts.template')

@section('title','Cập nhật video trang chủ')

@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    @if(Session::has('invalid'))
        <div class="alert alert-danger alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('invalid')}}
        </div>
    @endif
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('success')}}
        </div>
    @endif
    @if(isset($link) || $link != null)
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">{{ __('Video trang chủ') }}</h1>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary align-self-center">{{ __('Cập nhật video trang chủ') }}</h6>
            </div>
            <div class="card-body">
                <form method="post" action="{{ route('admin.youtube.update', ['id' => $id]) }}" enctype="multipart/form-data">
                    @csrf
                    <div style="width: 100%; display: flex; margin-bottom: 1rem">
                        <div class="row mr-2">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="link" 
                                        placeholder="Link Youtube" type="text" name="link" value="{{$link['link']}}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="title" 
                                        placeholder="Tiêu đề" type="text" name="title" value="{{$link['title']}}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" name="submit">{{ __('Cập nhật') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endif
</div>

@endsection