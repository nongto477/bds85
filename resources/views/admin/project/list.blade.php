@extends('admin.layouts.template')

@section('title','Danh sách dự án')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    @if(Session::has('invalid'))
        <div class="alert alert-danger alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('invalid')}}
        </div>
    @endif
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('success')}}
        </div>
    @endif
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">{{ __('Dự án') }}</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary align-self-center">{{ __('Danh sách dự án') }}</h6>
            <a href="{{ route('project.create.form') }}"><button class="btn btn-primary">{{ __('Thêm mới') }}</button></a>
        </div>
        <div class="card-body">
            <form class="form-inline mb-4" action="{{ route('project.search') }}" method="GET">
                <div class="form-group">
                    <label>{{ __('Chọn danh mục dự án:') }}</label>
                    <select class="form-control ml-2" name="q">
                      <option value="0" selected>{{ __('Biệt thự') }}</option>
                      <option value="1">{{ __('Biệt thự trệt') }}</option>
                      <option value="2">{{ __('Nhà phố') }}</option>
                      <option value="3">{{ __('Nhà cấp 4') }}</option>
                      <option value="4">{{ __('Công trình thi công') }}</option>
                      <option value="5">{{ __('Nội thất') }}</option>
                    </select>
                    <button type="submit" class="btn btn-primary ml-2">{{ __('Tìm kiếm') }}</button>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-hover" id="data-table">
                    <thead>
                        <tr class="text-primary">
                            <th>STT</th>
                            <th>{{ __('Danh mục dự án') }}</th>
                            <th>{{ __('Tiêu đề') }}</th>
                            <th>{{ __('Tiêu đề SEO') }}</th>
                            <th>{{ __('Lượt xem') }}</th>
                            <th class="text-center">{{ __('Thao tác') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $count = 1;
                        @endphp
                        @foreach ($projects as $item)
                            <tr>
                                <td>{{ $count }}</td>
                                <td>
                                    @php
                                        switch ($item['type_id']) {
                                            case "0":
                                                $title = __('Biệt thự');
                                                break;
                                            case "1":
                                                $title = __('Biệt thự trệt');
                                                break;
                                            case "2":
                                                $title = __('Nhà phố');
                                                break;
                                            case "3":
                                                $title = __('Nhà cấp 4');
                                                break;  
                                            case "4":
                                                $title = __('Công trình thi công');
                                                break;  
                                            case "5":
                                                $title = __('Nội thất');
                                                break;  
                                        }
                                        echo $title;
                                    @endphp  
                                </td>
                                <td>{{ $item['title'] }}</td>
                                <td>{{ $item['seo_title'] }}</td>
                                <td>{{ $item['view'] }}</td>
                                <td class="text-center manipulation">
                                    <a href="{{ route('project.edit.form',['id' => $item['id']]) }}"><i class="fas fa-edit"></i></a>
                                    <a href="{{ route('project.delete',['id' => $item['id']]) }}" onclick="return confirm('Bạn muốn xóa dự án này ?');"><i class="far fa-trash-alt"></i></a>
                                </td>
                            </tr>
                        @php
                            $count++;
                        @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection