@extends('admin.layouts.template')

@section('title', 'Thêm dự án')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    @if(Session::has('invalid'))
        <div class="alert alert-danger alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('invalid')}}
        </div>
    @endif
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('success')}}
        </div>
    @endif
    <a href="{{ route('project.list') }}"><i class="fas fa-arrow-left"></i> {{ __('Danh sách dự án') }}</a>
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">{{ __('Thêm dự án') }}</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="container form-text mt-3 mb-5">
            <div class="row">
                <div class="col-sm-12">
                    <!-- Form thêm dự án -->
                    <form method="post" action="{{ route('project.create') }}" enctype="multipart/form-data">

                        @csrf

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="keyword">{{ __('Danh mục dự án') }} <span class="text-danger">*</span></label>
                                    <select class="form-control" name="type_id" onmousedown="if(this.options.length > 5){this.size = 5;}"  onchange='this.size=0;' onblur="this.size=0;" required>
                                        <option value="0">{{ __('Biệt thự') }}</option>
                                        <option value="1">{{ __('Biệt thự trệt') }}</option>
                                        <option value="2">{{ __('Nhà phố') }}</option>
                                        <option value="3">{{ __('Nhà cấp 4') }}</option>
                                        <option value="4">{{ __('Công trình thi công') }}</option>
                                        <option value="5">{{ __('Nội thất') }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="title">{{ __('Tiêu đề') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="title" type="text" name="title" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="seo-title">{{ __('Tiêu đề SEO') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="seo-title" type="text" name="seo-title" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="keyword">{{ __('Từ khóa') }}</label>
                                    <input class="form-control" id="keyword" type="text" name="keyword">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="key-description">{{ __('Mô tả từ khóa') }}</label>
                                    <textarea class="form-control" type="text" id="key-description"
                                        name="key-description" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description">{{ __('Mô tả') }}</label>
                                    <textarea class="form-control" type="text" id="description"
                                        name="description" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="cnt">{{ __('Nội dung') }}</label>
                                    <textarea class="form-control" type="text" id="cnt"
                                        name="cnt" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="view">{{ __('Lượt xem') }}</label>
                                    <input class="form-control" id="view" type="text" name="view" value="0">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>{{ __('Thông tin dự án') }}</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="address">{{ __('Địa chỉ') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="address" type="text" name="address" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="area">{{ __('Diện tích (m2)') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="area" type="number" name="area" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="number_floor">{{ __('Số lầu') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="number_floor" type="number" name="number_floor" min=1 required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fee">{{ __('Chi phí') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="fee" type="number" name="fee" min=1>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="service">{{ __('Dịch vụ') }}</label>
                                    <input class="form-control" id="service" type="text" name="service">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="year_complete">{{ __('Năm hoàn thành') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="year_complete" type="number" name="year_complete" min=1990 required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label>{{ __('Hình ảnh dự án') }}</label>
                                <div id="multiple-images">
                                    <div id="add-image" class="add" onclick="addImage()">
                                        <i class="fas fa-plus"></i>
                                    </div>
                                </div>
                                <script>
                                    function addImage() {
                                        let imagePreview = $(`<div class="image-preview" style="display: none">
                                                <div class="overlay"></div>
                                                <div class="remove" onclick="removeImage(this)">
                                                    <i class="fas fa-trash"></i>
                                                    <input type="file" name="thumbnail[]" accept="image/*"
                                                        onchange="checkImage(this)" style="display: none">
                                                </div>
                                            </div>`);
                                        imagePreview.find('input').last().click();
                                    }
                                    function checkImage(el) {
                                        const file = el.files[0];
                                        if((file && file.size > 2097152) || !file) {
                                            alert("Dung lượng hình ảnh không vượt quá 2MB");
                                            return;
                                        }
                                        const reader = new FileReader();
                                        reader.addEventListener("load", function() {
                                            let imagePreview = $(el).parent().parent();
                                            imagePreview.attr("style", "display: flex; background-image: url(\""+this.result+"\")");
                                            $('#multiple-images #add-image').before(imagePreview);
                                        });
                                        reader.readAsDataURL(file);
                                    }
                                    function removeImage(el) {
                                        if($(el).parent().css('display') != 'none') {
                                            $(el).parent().remove();
                                        }
                                    }
                                </script>
                                <style>
                                    #multiple-images {
                                        display: grid;
                                        grid-template-columns: repeat(5, 1fr);
                                        grid-gap: .5rem;
                                        width: 100%;
                                        padding: 0;
                                        margin: 0;
                                        margin-bottom: 1rem;
                                    }
                                    #multiple-images > div {
                                        display: flex;
                                        align-items: start;
                                        justify-content: flex-end;
                                        padding: 0;
                                        margin: 0;
                                        background-color: black;
                                        border-radius: .5rem;
                                        overflow: hidden;
                                        background-size: cover;
                                        background-position: center center;
                                        width: 100%;
                                    }
                                    #multiple-images .overlay {
                                        width: 101%;
                                        padding-top: 101%;
                                        background-color: rgb(0, 0, 0, .2);
                                    }
                                    #multiple-images .remove {
                                        font-size: 1.2rem;
                                        padding: .5rem;
                                        color: white;
                                        cursor: pointer;
                                        position: absolute;
                                    }
                                    #multiple-images .add {
                                        display: flex;
                                        align-items: center;
                                        justify-content: center;
                                        width: 100%;
                                        padding-top: calc(50% - 1.5rem);
                                        padding-bottom: calc(50% - 1.5rem);
                                        font-size: 3rem;
                                        line-height: 3rem;
                                        color: gray;
                                        background: white;
                                        border: thin solid gray;
                                        cursor: pointer;
                                    }
                                </style>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" name="submit">{{ __('Tạo') }}</button>
                                <a href="{{ route('project.list') }}"><button type="button" class="btn btn-danger">{{ __('Quay lại') }}</button></a>
                            </div>
                        </div>
                    </form>
             </div>
        </div>
    </div>
</div>
@endsection