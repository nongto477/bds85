@extends('admin.layouts.template')

@section('title', 'Sản phẩm')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    @if(Session::has('invalid'))
        <div class="alert alert-danger alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('invalid')}}
        </div>
    @endif
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('success')}}
        </div>
    @endif
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">{{ __('Sản phẩm') }}</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary align-self-center">{{ __('Danh sách sản phẩm') }}</h6>
            <a href="{{ route('product.create.form') }}"><button class="btn btn-primary">{{ __('Thêm mới') }}</button></a>
        </div>
        <div class="card-body">
            <form class="form-inline mb-4" action="{{ route('product.search') }}" method="GET">
                <div class="form-group">
                    <label>{{ __('Chọn danh mục sản phẩm:') }}</label>
                    <select class="form-control ml-2" name="q">
                        @foreach ($categories as $item)
                            <option value="{{ $item['id'] }}">{{ $item['name'] }}</option>
                        @endforeach
                    </select>
                    <button type="submit" class="btn btn-primary ml-2">{{ __('Tìm kiếm') }}</button>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-hover" id="data-table">
                    <thead>
                        <tr class="text-primary">
                            <th>STT</th>
                            <th>{{ __('Mã sản phẩm') }}</th>
                            <th>{{ __('Tiêu đề') }}</th>
                            <th>{{ __('Tiêu đề SEO') }}</th>
                            <th>{{ __('Đơn giá') }}</th>
                            <th>{{ __('Lượt xem') }}</th>
                            <th>{{ __('Danh mục') }}</th>
                            <th class="text-center">{{ __('Thao tác') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $count = 1;
                        @endphp
                        @foreach ($products as $item)
                            <tr>
                                <td>{{ $count }}</td>
                                <td>{{ $item['sku'] }}</td>
                                <td>{{ $item['title'] }}</td>
                                <td>{{ $item['seo_title'] }}</td>
                                <td>{{ $item['price'] }}</td>
                                <td>{{ $item['view'] }}</td>
                                <td>{{ $item['category_name'] }}</td>
                                <td class="text-center manipulation">
                                    <a href="{{ route('product.edit.form',['sku' => $item['sku']]) }}"><i class="fas fa-edit"></i></a>
                                    <a href="{{ route('product.delete',['sku' => $item['sku']]) }}" onclick="return confirm('Bạn muốn xóa sản phẩm này ?');"><i class="far fa-trash-alt"></i></a>
                                </td>
                            </tr>
                        @php
                            $count++;
                        @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection