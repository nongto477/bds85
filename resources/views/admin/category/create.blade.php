@extends('admin.layouts.template')

@section('title', 'Thêm danh mục')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    @if(Session::has('invalid'))
        <div class="alert alert-danger alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('invalid')}}
        </div>
    @endif
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('success')}}
        </div>
    @endif
    <a href="{{ route('category.list') }}"><i class="fas fa-arrow-left"></i> {{ __('Danh sách danh mục sản phẩm') }}</a>
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">{{ __('Thêm danh mục') }}</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="container form-text mt-3 mb-5">
            <div class="row">
                <div class="col-sm-12">
                    <!-- Form thêm danh mục -->
                    <form method="post" action="{{ route('category.create') }}" enctype="multipart/form-data">

                        @csrf

                        <div class="row">
                            <div class="col-md-6">
                                <!-- Tên danh mục -->
                                <div class="form-group">
                                    <label for="category-name">{{ __('Tên danh mục') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="category-name" type="text" name="category-name" required />
                                </div>
                            </div><div class="col-md-6">
                                <div class="form-group">
                                    <label for="seo-title">{{ __('Tiêu đề SEO') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="seo-title" type="text" name="seo-title" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="keyword">{{ __('Từ khóa') }}</label>
                                    <input class="form-control" id="keyword" type="text" name="keyword">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="key-description">{{ __('Mô tả từ khóa') }}</label>
                                    <textarea class="form-control" type="text" id="key-description"
                                        name="key-description" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" name="submit">
                                    {{ __('Tạo') }}
                                </button>
                                <a href="{{ route('category.list') }}"><button type="button" class="btn btn-danger">{{ __('Quay lại') }}</button></a>
                            </div>
                        </div>
                    </form>
             </div>
        </div>
    </div>
</div>
@endsection