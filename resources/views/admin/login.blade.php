<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ __('Login') }}</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <!-- Css -->
    <link rel="stylesheet" href="{{asset('admin/assets/css/login.css')}}" media="all" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="{{asset('favicon.ico')}}" />
    <style>
      .container > .alert:nth-child(1) {
        margin-top: 1rem;
      }
    </style>
</head>
<body>
    <div class="container">
      @if(Session::has('invalid'))
      <div class="alert alert-danger alert-dismissible">
           <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
           {{Session::get('invalid')}}
      </div>
      @endif
      @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('success')}}
            </div>
      @endif
        <div class="row">
          <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin my-5">
              <div class="card-body">
                <h5 class="card-title text-center">{{ __('Quản trị viên') }}</h5>
                <form class="form-signin" action="{{ route('handle.login') }}" method="POST" enctype="multipart/form-data">

                  @csrf
                  
                  <div class="form-label-group">
                    <input type="email" id="email" class="form-control" placeholder="email" name="email" required autofocus>
                    <label for="email">{{ __('Email') }}</label>
                  </div>
    
                  <div class="form-label-group">
                    <input type="password" id="password" class="form-control" placeholder="password" name="password" required autofocus>
                    <label for="password">{{ __('Password') }}</label>
                  </div>
                  <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">{{ __('Đăng nhập') }}</button>
                </form>
              </div>
            </div>
          </div>
        </div>
    </div>
</body>
</html>