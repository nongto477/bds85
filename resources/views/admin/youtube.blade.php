@extends('admin.layouts.template')

@section('title','Video trang chủ')

@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    @if(Session::has('invalid'))
        <div class="alert alert-danger alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('invalid')}}
        </div>
    @endif
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('success')}}
        </div>
    @endif
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">{{ __('Video trang chủ') }}</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary align-self-center">{{ __('Danh sách link video') }}</h6>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('admin.youtube.create') }}" enctype="multipart/form-data">
                @csrf
                <div style="width: 100%; display: flex; margin-bottom: 1rem">
                    <div class="row mr-2">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input class="form-control" id="link" 
                                    placeholder="Thêm link Youtube" type="text" name="link" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input class="form-control" id="title" 
                                    placeholder="Tiêu đề" type="text" name="title" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary" name="submit">{{ __('Thêm') }}</button>
                        </div>
                    </div>
                </div>
            </form>

            <div class="table-responsive">
                <table class="table table-hover" id="data-table">
                    <thead>
                        <tr class="text-primary">
                            <th>{{ __('Link') }}</th>
                            <th>{{ __('Tiêu đề') }}</th>
                            <th class="text-center">{{ __('Thao tác') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($links as $item)
                            <tr>
                                <td>{{ $item['link'] }}</td>
                                <td>{{ $item['title'] }}</td>
                                <td class="text-center manipulation">
                                    <a href="{{ route('admin.youtube.edit', ['id' => $item['id']]) }}"><i class="fas fa-edit"></i></a>
                                    <a href="{{ route('admin.youtube.remove', ['id' => $item['id']]) }}" onclick="return confirm('Bạn muốn xóa link này ?');"><i class="far fa-trash-alt"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection