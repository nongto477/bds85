@extends('admin.layouts.template')

@php
    if($q == 0) {
        $title = 'Kiến thức';
    }elseif($q == 1) {
        $title = 'Giới thiệu';
    }elseif($q == 2) {
        $title = 'Tin tức';
    }else {
        $title = 'Bảng giá';
    }
@endphp

@section('title',$title)

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">{{ __('Bài viết') }}</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary align-self-center">{{ __('Danh sách bài viết') }}</h6>
            <a href="{{ route('article.create.form') }}"><button class="btn btn-primary">{{ __('Thêm mới') }}</button></a>
        </div>
        <div class="card-body">
            <form class="form-inline mb-4" action="{{ route('article.search') }}" method="GET">
                <div class="form-group">
                    <label>{{ __('Chọn danh mục bài viết:') }}</label>
                    <select class="form-control ml-2" name="q">
                      <option value="0" {{ $q == 0 ? 'selected':'' }}>{{ __('Kiến thức') }}</option>
                      <option value="1" {{ $q == 1 ? 'selected':'' }}>{{ __('Giới thiệu') }}</option>
                      <option value="2" {{ $q == 2 ? 'selected':'' }}>{{ __('Tin Tức') }}</option>
                      <option value="3" {{ $q == 3 ? 'selected':'' }}>{{ __('Bảng giá') }}</option>
                    </select>
                    <button type="submit" class="btn btn-primary ml-2">{{ __('Tìm kiếm') }}</button>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-hover" id="data-table">
                    <thead>
                        <tr class="text-primary">
                            <th>STT</th>
                            <th>{{ __('Danh mục bài viết') }}</th>
                            <th>{{ __('Hình ảnh') }}</th>
                            <th>{{ __('Tiêu đề') }}</th>
                            <th>{{ __('Tiêu đề SEO') }}</th>
                            <th>{{ __('Lượt xem') }}</th>
                            <th class="text-center">{{ __('Thao tác') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $count = 1;
                        @endphp
                        @foreach ($articles as $item)
                            <tr>
                                <td>{{ $count }}</td>
                                <td>
                                    @php
                                        $title = "";
                                        switch ($item['type_id']) {
                                            case "0":
                                                $title = __('Kiến thức');
                                                break; 
                                            case "1":
                                                $title = __('Giới thiệu');
                                                break; 
                                            case "2":
                                                $title = __('Tin tức');
                                                break; 
                                            case "3":
                                                $title = __('Bảng giá');
                                                break; 
                                        }
                                        echo $title;
                                    @endphp  
                                </td>
                                <td><a href="{{ asset('images/articles/x400/'.$item['thumbnail']) }}" target="_blank"><img src="{{ asset('images/articles/x400/'.$item['thumbnail']) }}" width=60/></a></td>
                                <td>{{ $item['title'] }}</td>
                                <td>{{ $item['seo_title'] }}</td>
                                <td>{{ $item['view'] }}</td>
                                <td class="text-center manipulation">
                                    <a href="{{ route('article.edit.form',['id' => $item['id']]) }}"><i class="fas fa-edit"></i></a>
                                    <a href="{{ route('article.delete',['id' => $item['id']]) }}" onclick="return confirm('Bạn muốn xóa bài viết này ?');"><i class="far fa-trash-alt"></i></a>
                                </td>
                            </tr>
                        @php
                            $count++;
                        @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection