@extends('admin.layouts.template')

@section('title', 'Cập nhật bài viết')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    @if(Session::has('invalid'))
        <div class="alert alert-danger alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('invalid')}}
        </div>
    @endif
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('success')}}
        </div>
    @endif
    <a href="{{ route('article.list') }}"><i class="fas fa-arrow-left"></i> {{ __('Danh sách bài viết') }}</a>
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">{{ __('Cập nhật bài viết') }}</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="container form-text mt-3 mb-5">
            <div class="row">
                <div class="col-sm-12">
                    <!-- Form thêm sản phẩm -->
                    <form method="post" action="{{ route('article.edit',['id' => $article['id']]) }}" enctype="multipart/form-data">

                        @csrf

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="keyword">{{ __('Danh mục bài viết') }} <span class="text-danger">*</span></label>
                                    <select class="form-control" name="type_id" required>
                                        <option value="0" {{ $article['type_id'] === 0 ? 'selected':'' }}>{{ __('Kiến thức') }}</option>
                                        <option value="1" {{ $article['type_id'] === 1 ? 'selected':'' }}>{{ __('Giới thiệu') }}</option>
                                        <option value="2" {{ $article['type_id'] === 2 ? 'selected':'' }}>{{ __('Tin tức') }}</option>
                                        <option value="3" {{ $article['type_id'] === 3 ? 'selected':'' }}>{{ __('Bảng giá') }}</option>
                                    </select>
                                    <small class="text-danger">{{ __('Lưu ý: danh mục bài viết là "Giới thiệu" chỉ có duy nhất 1 bài viết.') }}</small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="title">{{ __('Tiêu đề') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="title" type="text" name="title" value="{{ $article['title'] }}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="seo-title">{{ __('Tiêu đề SEO') }} <span class="text-danger">*</span></label>
                                    <input class="form-control" id="seo-title" type="text" name="seo-title" value="{{ $article['seo_title'] }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="keyword">{{ __('Từ khóa') }}</label>
                                    <input class="form-control" id="keyword" type="text" name="keyword" value="{{ $article['keyword'] }}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="key-description">{{ __('Mô tả từ khóa') }}</label>
                                    <textarea class="form-control" type="text" id="key-description"
                                        name="key-description" rows="3">{{ $article['key_description'] }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description">{{ __('Mô tả') }}</label>
                                    <textarea class="form-control" type="text" id="description"
                                        name="description" rows="3">{{ $article['description'] }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="cnt">{{ __('Nội dung') }}</label>
                                    <textarea class="form-control" type="text" id="cnt"
                                        name="cnt" rows="3">{{ $article['content'] }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="view">{{ __('Lượt xem') }}</label>
                                    <input class="form-control" id="view" type="text" name="view" value="{{ $article['view'] }}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="thumbnail">{{ __('Thumbnail (tối đa 2MB)') }}</label>
                                    <div class="custom-file">
                                        <input type="file" id="thumbnail" name="thumbnail"
                                            accept=".png,.gif,.jpg,.jpeg">
                                    </div>
                                    <div id="error" class="text-danger"></div>
                                </div>
                                <div class="image-preview mb-4" id="imagePreview">
                                    <img src="{{ asset('images/articles/x400/'.$article['thumbnail']) }}" alt="Image Preview" class="image-preview__image" style="display:block;"/>
                                    <span class="image-preview__default-text" style="display:none">{{ __('Hình ảnh') }}</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" name="submit">{{ __('Cập nhật') }}</button>
                                <a href="{{ route('article.list') }}"><button type="button" class="btn btn-danger">{{ __('Quay lại') }}</button></a>
                            </div>
                        </div>
                    </form>
             </div>
        </div>
    </div>
</div>
@endsection