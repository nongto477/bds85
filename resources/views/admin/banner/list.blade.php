@extends('admin.layouts.template')

@section('title','Banner')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    @if(Session::has('invalid'))
        <div class="alert alert-danger alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('invalid')}}
        </div>
    @endif
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('success')}}
        </div>
    @endif
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">{{ __('Banner') }}</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary align-self-center">{{ __('Danh sách banner') }}</h6>
            <a href="{{ route('banner.create.form') }}"><button class="btn btn-primary">{{ __('Thêm mới') }}</button></a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover" id="data-table">
                    <thead>
                        <tr class="text-primary">
                            <th>STT</th>
                            <th>{{ __('Hình ảnh') }}</th>
                            <th class="text-center">{{ __('Thao tác') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $count = 1;
                        @endphp
                        @foreach ($banners as $item)
                            <tr>
                                <td>{{ $count }}</td>
                                <td>
                                    <a href="{{ asset('images/banners/x1200/'.$item['thumbnail']) }}" target="_blank"><img src="{{ asset('images/banners/'.$item['thumbnail']) }}" width=150/></a>
                                </td>
                                <td class="text-center manipulation">
                                    <a href="{{ route('banner.edit.form',['id' => $item['id']]) }}"><i class="fas fa-edit"></i></a>
                                    <a href="{{ route('banner.delete',['id' => $item['id']]) }}" onclick="return confirm('Bạn muốn xóa banner này ?');"><i class="far fa-trash-alt"></i></a>
                                </td>
                            </tr>
                        @php
                            $count++;
                        @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection