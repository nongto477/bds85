@extends('admin.layouts.template')

@section('title', 'Thêm banner')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    @if(Session::has('invalid'))
        <div class="alert alert-danger alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('invalid')}}
        </div>
    @endif
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('success')}}
        </div>
    @endif
    <a href="{{ route('banner.list') }}"><i class="fas fa-arrow-left"></i> {{ __('Danh sách banner') }}</a>
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Thêm banner</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="container form-text mt-3 mb-5">
            <div class="row">
                <div class="col-sm-12">
                    <!-- Form thêm banner -->
                    <form method="post" action="{{ route('banner.create') }}" enctype="multipart/form-data">

                        @csrf

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="thumbnail">{{ __('Hình ảnh (tối đa 2MB)') }} <span class="text-danger">*</span></label>
                                    <div class="custom-file">
                                        <input type="file" id="thumbnail" name="thumbnail" accept=".png,.gif,.jpg,.jpeg" required>
                                    </div>
                                    <div id="error" class="text-danger"></div>
                                </div>
                                <div class="image-preview mb-4" id="imagePreview">
                                    <img src="" alt="Image Preview" class="image-preview__image" />
                                    <span class="image-preview__default-text">{{ __('Hình ảnh') }}</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" name="submit">
                                    {{ __('Tạo') }}
                                </button>
                                <a href="{{ route('banner.list') }}"><button type="button" class="btn btn-danger">{{ __('Quay lại') }}</button></a>
                            </div>
                        </div>
                    </form>
             </div>
        </div>
    </div>
</div>
@endsection