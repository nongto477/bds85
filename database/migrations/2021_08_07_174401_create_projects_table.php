<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('seo_title');
            $table->string('keyword')->nullable();
            $table->text('key_description')->nullable();
            $table->text('description')->nullable();
            $table->text('content')->nullable();
            $table->integer('view')->default(0);
            $table->integer('type_id');
            $table->string('slug');
            $table->text('address')->nullable();
            $table->integer('area')->default(0);
            $table->integer('number_floor')->default(0);
            $table->float('fee')->nullable();
            $table->integer('service_id');
            $table->integer('year_complete');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('projects');
    }
}
