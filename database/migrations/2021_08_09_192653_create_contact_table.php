<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('contact', function (Blueprint $table) {
            $table->id();
            $table->string("name_company");
            $table->string("address");
            $table->string("phone");
            $table->string("phone_sale");
            $table->string("email");
            $table->string("name_contact");
            $table->string("phone_contact");
            $table->string("google_map");
            $table->string("youtube");
            $table->string("zalo");
            $table->string("title");
            $table->longText("desc");
            $table->longText("keywords");
            $table->longText("desc_video_projects");
            $table->longText("desc_info");
            $table->string("favicon");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('contact');
    }
}
