<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePriceListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('price_list', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('seo_title');
            $table->string('keyword')->nullable();
            $table->text('key_description')->nullable();
            $table->text('description')->nullable();
            $table->text('content')->nullable();
            $table->integer('type_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('price_list');
    }
}
