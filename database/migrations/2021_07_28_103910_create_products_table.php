<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('seo_title');
            $table->string('keyword')->nullable();
            $table->text('key_description')->nullable();
            $table->text('description')->nullable();
            $table->text('content')->nullable();
            $table->string('sku');
            $table->integer('price');
            $table->string('slug');
            $table->bigInteger('category_id')->unsigned()->index();;
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('products');
    }
}
