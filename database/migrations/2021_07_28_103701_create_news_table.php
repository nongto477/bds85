<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->text('thumbnail')->nullable();
            $table->string('title');
            $table->string('seo_title');
            $table->string('keyword')->nullable();
            $table->text('key_description')->nullable();
            $table->text('description')->nullable();
            $table->text('content')->nullable();
            $table->integer('view')->default(0);
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('news');
    }
}
