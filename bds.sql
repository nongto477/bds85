-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 01, 2021 at 11:11 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bds`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `thumbnail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyword` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view` int(11) NOT NULL DEFAULT 0,
  `type_id` int(11) NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `thumbnail`, `title`, `seo_title`, `keyword`, `key_description`, `description`, `content`, `view`, `type_id`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'article_chao-mung-quy-khach-den-voi-cong-ty-kien-truc-de-design_1629447459872.jpg', 'Chào mừng quý khách đến với công ty kiến trúc D.E DESIGN', 'Chào mừng quý khách đến với công ty kiến trúc D.E DESIGN', '', '', '', '<p style=\"text-align:justify\"><span style=\"color:rgb(255, 0, 0)\">D.E Design</span>&nbsp;là công ty hoạt động trong lĩnh vực thiết kế và xây dựng, chuyên về thiết kế biệt thự, nhà phố, nhà cấp 4, công trình công cộng.</p>\r\n\r\n<p style=\"text-align:justify\">Trong nhiều năm qua, cùng với sự phát triển về kiến trúc,&nbsp;<span style=\"color:rgb(255, 0, 0)\">D.E Design</span>&nbsp;đã và đang khẳng định được vị trí, chất lượng dịch vụ và uy tín của công ty đến với khách hàng. Với cơ sở vật chất hiện đại, đội ngũ kiến trúc sư được đào tạo bài bản, có nhiều năm kinh nghiệm cùng sự sáng tạo, luôn tận tâm với công việc để tạo ra những không gian đẹp, những công trình đẹp đến quý khách hàng.</p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:rgb(255, 0, 0)\">D.E Design</span>&nbsp;luôn nghiên cứu những thiết kế mới lạ, phù hợp với xu hướng hiện đại kết hợp với yếu tố phong thủy, am hiểu về phong thủy để thu hút vận may cũng như thu hút tài lộc của gia chủ. Các thiết kế được nghiên cứu chuyên sâu để tạo ra một thế kiết nhà đẹp phù hợp với phong thủy không chỉ hài hòa từ hướng nhà, hướng cửa mà còn tổng thể nội thất bên trong hay phối cảnh xung quanh nhà.</p>\r\n\r\n<p style=\"text-align:justify\"><img alt=\"\" src=\"https://bds85.lc/uploads/product_image_1629447218477.jpg\" style=\"height:405px; width:400px\" /><img alt=\"\" src=\"https://bds85.lc/uploads/product_image_1629447247579.jpg\" style=\"height:405px; width:400px\" /></p>\r\n\r\n<p style=\"text-align:justify\">Sau nhiều năm nỗ lực,&nbsp;<span style=\"color:rgb(255, 0, 0)\">D.E Design</span>&nbsp;đã trở thành lựa chọn hàng đầu của nhiều quý khách, chúng tôi thấu hiểu được nhu cầu cũng như mong muốn của khách hàng, chính vì vậy luôn làm khách hàng yên tâm và hài lòng bằng chính những dịch vụ thiết kế và thi công chuyên nghiệp.</p>\r\n\r\n<p style=\"text-align:justify\">Với tiêu chí chuyên nghiệp trong công việc, tận tâm trong dịch vụ và chu đáo khi hậu mãi, Công ty kiến trúc&nbsp;<span style=\"color:rgb(255, 0, 0)\">D.E Design</span>&nbsp; luôn là một nhà thiết kế và nhà thầu chính được tín nhiệm hiện nay.&nbsp;<span style=\"color:rgb(255, 0, 0)\">D.E Design</span>&nbsp;đã thiết kế và thi công tất cả các hạng mục của hơn 1000 công trình các loại như Biệt Thự, Cao ốc Văn Phòng, Siêu Thị, Khách Sạn, Nhà Phố, …</p>\r\n\r\n<p style=\"text-align:justify\"><img alt=\"\" src=\"https://bds85.lc/uploads/product_image_1629447278776.jpg\" style=\"height:405px; width:400px\" /><img alt=\"\" src=\"https://bds85.lc/uploads/product_image_1629447303162.jpg\" style=\"height:405px; width:400px\" /></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-family:segoe ui; font-size:15px\">Mục tiêu chính của công ty chúng tôi, là đem lại sự hài lòng cho quý khách với tiêu chí “Chuyên nghiệp – Chất lượng cao – Giá cả phải chăng”.&nbsp;Uy tín là vấn đề hàng đầu mà&nbsp;</span><span style=\"font-family:segoe ui; font-size:15px\">D.E Design</span><span style=\"font-family:segoe ui; font-size:15px\">&nbsp;đặt ra để đưa tới cho quý khách hàng một sản phẩm tốt nhất. Sự tin tưởng và ủng hộ của khách hàng trong suốt thời gian qua là động lực to lớn trên bước đường phát triển của&nbsp;</span><span style=\"font-family:segoe ui; font-size:15px\">D.E Design</span><span style=\"color:rgb(255, 255, 255); font-family:segoe ui; font-size:15px\">.</span></p>\r\n\r\n<p style=\"text-align:justify\"><img alt=\"\" src=\"https://bds85.lc/uploads/product_image_1629447422293.jpg\" style=\"height:405px; width:400px\" /><img alt=\"\" src=\"https://bds85.lc/uploads/product_image_1629447438222.jpg\" style=\"height:405px; width:400px\" /></p>', 0, 1, 'chao-mung-quy-khach-den-voi-cong-ty-kien-truc-de-design', '2021-08-20 01:17:40', '2021-08-20 01:17:40'),
(2, 'article_bang-gia-thi-cong_1629448605436.jpg', 'Bảng giá thi công', 'bang-gia-thi-cong', '', '', '', '<p><img alt=\"\" src=\"https://bds85.lc/uploads/product_image_1629448583749.jpg\" style=\"height:1591px; width:1500px\" /></p>', 1, 3, 'bang-gia-thi-cong', '2021-08-20 01:36:45', '2021-08-20 01:37:54'),
(3, 'article_bang-gia-thiet-ke_1629448652498.jpg', 'Bảng giá thiết kế', 'bang-gia-thiet-ke', '', '', '', '<p><img alt=\"\" src=\"https://bds85.lc/uploads/product_image_1629448634734.jpg\" style=\"height:1900px; width:979px\" /></p>', 0, 3, 'bang-gia-thiet-ke', '2021-08-20 01:37:32', '2021-08-20 01:37:32'),
(4, 'article_9-cach-de-tan-dung-toi-da-khong-gian-san-hien-nha-ban_1629448917656.jpg', '9 cách để tận dụng tối đa không gian sân hiên nhà bạn', '9 cách để tận dụng tối đa không gian sân hiên nhà bạn', '', '', 'Bởi sân trong thường khá nhỏ, nên khá đáng tiền để trang hoàng chúng như một căn phòng thật sự, với sàn nhà, sơn màu và chỗ ngồi - tạo ra một khu vực ngoài trời đầy hấp dẫn. Hãy xem những ý tưởng dưới đây để tối đa hóa không gian và ánh sáng.', NULL, 1, 0, '9-cach-de-tan-dung-toi-da-khong-gian-san-hien-nha-ban', '2021-08-20 01:41:57', '2021-08-30 23:15:48'),
(5, 'article_9-meo-can-biet-de-co-the-mix-and-match-gach-lat-san_1629460907488.jpg', '9 mẹo cần biết để có thể “mix and match” gạch lát sàn', '9 mẹo cần biết để có thể “mix and match” gạch lát sàn', '', '', 'Đối với những fan hâm mộ gạch như tôi, càng nhiều gạch thì càng tốt. Nhưng mặc dù tôi rất vui khi mọi mặt phẳng đều được lát gạch, vẫn có một số điều cần cân nhắc khi kết hợp các loại gạch với nhiều kích cỡ, hình dạng và hoa văn khác nhau trên tường và sàn nhà. Vì tốn kha khá tiền khi bạn lát gạch một bề mặt lớn, nên việc mua một vài viên gạch mẫu và lát thử xem chúng có hợp nhau không rất đáng được thực hiện. Chín mẹo dưới đây sẽ hướng dẫn cho bạn về cách kết hợp gạch để có được sự hoản hảo.', NULL, 0, 0, '9-meo-can-biet-de-co-the-mix-and-match-gach-lat-san', '2021-08-20 05:01:47', '2021-08-20 05:01:47'),
(6, 'article_tokyo-city-guide-25-cong-trinh-bieu-tuong-cua-thu-do-nhat-ban_1629461201822.jpg', 'Tokyo City Guide: 25 công trình biểu tượng của thủ đô Nhật bản', 'Tokyo City Guide: 25 công trình biểu tượng của thủ đô Nhật bản', '', '', 'Dưới đây là tập hợp 25 công trình đáng chú ý ở Tokyo. Một chỉ dẫn hữu ích cho bạn một điểm khởi đầu trong chuyến hành trình đến thành phố lớn nhất của Nhật Bản, gắn với tên tuổi các công ty thiết kế, kiến trúc sư nổi tiếng như Nikken Sekkei, Herzog & De Meuron, Toyo Ito, Kengo Kuma, Sou Fujimoto, Kenzo Tange, OMA và Kazuyo Sejima.', NULL, 0, 0, 'tokyo-city-guide-25-cong-trinh-bieu-tuong-cua-thu-do-nhat-ban', '2021-08-20 05:06:41', '2021-08-20 05:06:41'),
(7, 'article_phan-biet-coverlet-duvet-quilt-va-comforter-ma-ban-can-biet_1629465618024.jpg', 'Phân biệt Coverlet, Duvet, Quilt và Comforter mà bạn cần biết', 'Phân biệt Coverlet, Duvet, Quilt và Comforter mà bạn cần biết', '', '', 'Sử dụng nguyên bộ chăn ga gối đệm đẹp đẽ có thể hơi quá sức - và đắt đỏ. Nhưng, chỉ với một vài lựa chọn khi đi mua sắm và sử dụng một cách thấu đáo các món đồ bạn đang sở hữu có thể dễ dàng có được một chiếc giường tuyệt đẹp. Tất nhiên, trở ngại đầu tiên chính là hiểu được định nghĩa của các khái niệm sau: bedspread (ga trải giường), coverlet (khăn phủ giường), quilt (chăn mỏng/chăn thu), duvet (chăn có vỏ và ruột tách rời), comforter (chăn có vỏ và ruột dính liền) - chúng có ý nghĩa gì, bạn phải sử dụng chúng như thế nào? Hãy cùng tìm hiểu nhé!', NULL, 0, 0, 'phan-biet-coverlet-duvet-quilt-va-comforter-ma-ban-can-biet', '2021-08-20 06:20:18', '2021-08-20 06:20:18'),
(8, 'article_du-an-be-chua-carbon-trong-toa-nha-cao_1629465778137.jpg', 'Dự án bể chứa carbon trong tòa nhà cao', 'Dự án bể chứa carbon trong tòa nhà cao', '', '', 'Công ty kiến trúc có trụ sở tại Paris, Rescubika đã đề xuất ý tưởng xây dựng một tòa nhà chọc trời, cao tới 737m giúp làm sạch không khí và giảm lượng khí thải của đô thị trong tương lai.', NULL, 0, 0, 'du-an-be-chua-carbon-trong-toa-nha-cao', '2021-08-20 06:22:58', '2021-08-20 06:22:58'),
(9, 'article_cong-ty-kien-truc-co-tru-so-tai-paris-rescubika-da-de-xuat-y-tuong-xay-dung-mot-toa-nha-choc-troi-cao-toi-737m-giup-lam-sach-khong-khi-va-giam-luong-khi-thai-cua-do-thi-trong-tuong-lai_1629465830840.jpg', 'Công ty kiến trúc có trụ sở tại Paris, Rescubika đã đề xuất ý tưởng xây dựng một tòa nhà chọc trời, cao tới 737m giúp làm sạch không khí và giảm lượng khí thải của đô thị trong tương lai.', 'Công ty kiến trúc có trụ sở tại Paris, Rescubika đã đề xuất ý tưởng xây dựng một tòa nhà chọc trời, cao tới 737m giúp làm sạch không khí và giảm lượng khí thải của đô thị trong tương lai.', '', '', 'Hà Lan được biết đến là quốc gia xuất khẩu nông nghiệp lớn thứ hai thế giới, chỉ đứng sau Mỹ - quốc gia có diện tích đất liền lớn hơn gấp 237 lần. Đây là thành tựu đánh nể của Hà Lan khi xét đến mức độ chênh lệch diện tích giữa hai nước. Dù là đất nước nhỏ bé, song khối lượng sản vật nông nghiệp xuất khẩu hàng năm của đất nước này luôn được duy trì ở mức rất cao, riêng năm 2017, giá trị xuất khẩu nông nghiệp lên tới 100 tỉ đô la Mỹ, kèm theo đó là 10 tỉ đô la thu được từ những sản phẩm ứng dụng trong sản xuất nông nghiệp. Chìa khóa đằng sau thành công trong sản xuất nông nghiệp của Hà Lan nằm ở việc áp dụng những thành tựu trong thiết kế kiến trúc trong việc quy hoạch nông nghiệp.\r\n\r\nĐể tạo ra năng suất cao, người Hà Lan đã chủ yếu sử dụng nhà kính trong sản xuất nông nghiệp, tạo thành những khối nhà khổng lồ trải khắp vùng ngoại ô phía Nam Hà Lan, một số khu vực có diện tích lên tới 175 mẫu Anh, tiêu biểu như có thể thấy được qua bộ sưu tập ảnh của Tom Hegen mang tên The Green house series. Tính trên cả nước, khu vực diện tích của những khối nhà kính này lên tới 58 km vuông, lớn hơn 56% tổng diện tích của đảo Manhattan – Mỹ.', NULL, 0, 0, 'cong-ty-kien-truc-co-tru-so-tai-paris-rescubika-da-de-xuat-y-tuong-xay-dung-mot-toa-nha-choc-troi-cao-toi-737m-giup-lam-sach-khong-khi-va-giam-luong-khi-thai-cua-do-thi-trong-tuong-lai', '2021-08-20 06:23:50', '2021-08-20 06:23:50'),
(10, 'article_xay-nha-gia-500-trieu-co-xay-du-khong_1629487476071.jpg', 'Xây nhà giá 500 triệu có xây đủ không?', 'Xây nhà giá 500 triệu có xây đủ không?', '', '', '', '<p><strong>Hỏi: Tôi có mảnh đất 96m2 tại thành phố Hồ Chí Minh, gia đình gồm 3 người thì xây nhà như thế nào cho hợp lý chỉ với 500 triệu?</strong><br />\r\n<br />\r\n<strong>Đáp</strong><span style=\"color:rgb(0, 0, 0); font-family:arial,helvetica,sans-serif; font-size:16px\">: Chào bạn, Hồ Chí Minh khá nóng và mưa cũng nhiều, vì thế chúng tôi xin được tư vấn như sau:</span><br />\r\n<br />\r\n<span style=\"color:rgb(0, 0, 0); font-family:arial,helvetica,sans-serif; font-size:16px\">-&nbsp;&nbsp; &nbsp;Bạn không nên xây nhà cao tầng vì khí hậu sẽ khá nóng phả vào tường dù bạn có dùng biện pháp chống nóng.</span><br />\r\n<span style=\"color:rgb(0, 0, 0); font-family:arial,helvetica,sans-serif; font-size:16px\">-&nbsp;&nbsp; &nbsp;Thiết kế theo kiểu villa có gác lửng làm phòng thờ, trần nhà cao lên sẽ có tác dụng chống nóng cực tốt.</span><br />\r\n<span style=\"color:rgb(0, 0, 0); font-family:arial,helvetica,sans-serif; font-size:16px\">-&nbsp;&nbsp; &nbsp;Cấu trúc bên trong gồm 2 phòng ngủ, 2 nhà vệ sinh, 1 phòng khách, bếp và ăn sẽ chung 1 khu.</span></p>', 0, 2, 'xay-nha-gia-500-trieu-co-xay-du-khong', '2021-08-20 12:24:37', '2021-08-20 12:24:37'),
(11, 'article_nhung-luu-y-khi-xay-dung-nha-cap-4_1629487550066.jpg', 'NHỮNG LƯU Ý KHI XÂY DỰNG NHÀ CẤP 4', 'NHỮNG LƯU Ý KHI XÂY DỰNG NHÀ CẤP 4', '', '', '', '<p style=\"text-align:justify\"><strong><span style=\"font-size:14px\"><em>Những lưu ý trước khi xây nhà là vô cùng quan trọng</em></span></strong></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">1. Xác định chính xác mục tiêu sử dụng: Để ở đơn thuần, để ở kết hợp kinh doanh, để kinh doanh, cho thuê, để ở nghỉ dưỡng....</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">2. Xác định nhu cầu cơ bản của gia đình khi xây nhà ở dân dụng như: số lượng phòng, diện tích và vị trí của các phòng, phong cách và vật dụng trang trí nội thất sẽ sử dụng, không gian dự trữ, phòng thờ, nhà xe, vườn nhỏ, sân phơi, bồn chứa nước...</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">3. Lưu ý về những thay đổi trong tương lai, ví dụ như đám cưới và gia đình sẽ có thêm người, v..v...</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">4. Nên tham khảo tất cả các thành viên trong gia đình trước khi thông qua những kế hoạch lần cuối.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">5. Sắp xếp theo thứ tự ưu tiên tất cả thông tin được đề cập ở trên.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">6. Tập hợp và ghi lại tất cả các thông tin ở trên khi lập kế hoạch xây dựng nhà phố của bạn để sau này làm việc với kiến trúc sư.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">Khi xây dựng nhà ở cây xanh trồng ở lối đi bộ đem lại bóng râm cho đường phố và vỉa hè, giúp giảm nhiệt độ và tăng giá trị cảnh quan. Ngoài ra, cây xanh cũng mang lại những lợi ích bổ sung như:</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Che nắng cho khu vực để xe dọc đường sau khi xây dựng nhà xong</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Khu vực xanh nhỏ có thể là không gian chung cho các hoạt động cộng đồng</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">Khu vực có nhiều cây xanh có thể hỗ trợ cho những người bán hàng rong. Bằng cách này, nhiều người sẽ có cơ hội trở nên quen thuộc với khu vực này và góp phần làm tăng sức sống đô thị.</span></p>\r\n\r\n<p style=\"text-align:justify\"><strong><span style=\"font-size:14px\">Tìm hiểu thủ tục pháp lý</span></strong></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">Để được cấp giấy phép xây dựng cần phải có những loại giấy tờ nào?</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">Người xin cấp giấy phép xây dựng phải có một trong những loại giấy tờ sau:</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Giấy chứng nhận quyền sử dụng đất (Giấy đỏ), nếu xây dựng nhà ở dân dụng trên nền đất trống.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Giấy chứng nhận Quyền sở hữu nhà ở &amp; Quyền sử dụng đất ở (Giấy hồng) hoặc Giấy chứng nhận sở hữu nhà, nếu làm nhà trên nền nhà cũ.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Quyết định giao đất, cho thuê đất của các cơ quan Nhà nước có thẩm quyền.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Giấy chứng nhận quyền sử dụng đất tạm thời do cơ quan có thẩm quyền cấp hoặc có tên trong Sổ địa chính mà không có tranh chấp.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Giấy tờ do cơ quan có thẩm quyền thuộc chế độ cũ cấp (gồm: Bằng khoán điền thổ hoặc Trích lục, Trích sao bản đồ điền thổ, Bản đồ phân chiếc thửa, Chứng thư đoạn mãi đã thị thực, đăng tịch, sang tên tại Văn phòng chưởng khế, Ty điền địa, Nha trước bạ).</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Giấy tờ thừa kế nhà, đất được UBND phường xã xác nhận về thừa kế, không có tranh chấp.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Bản án hoặc Quyết định của Tòa án nhân dân hoặc Quyết định giải quyết tranh chấp đất đai đã có hiệu lực pháp luật.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Giấy tờ chuyển nhượng đất đai, mua bán nhà ở kèm theo quyền sử dụng đất ở được UBND Phường/Xã/Thị trấn, Quận/Huyện xác nhận không có tranh chấp.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Các loại giấy tờ hợp lệ về nhà ở, đất ở theo Quyết định 38/2000/QĐ-UB-ĐT ngày 19-6-2000 của UBND Thành phố.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">Hồ sơ xin cấp giấy phép xây dựng nhà ở được quy định như thế nào?</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">Hồ sơ xin cấp giấy phép xây dựng nhà phố mới được lập thành 03 bộ và nộp tại UBND Quận/Huyện gồm:</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Đơn xin cấp giấy phép xây dựng nhà ở (theo mẫu) do chủ nhà đứng tên.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Bản sao có thị thực một trong những giấy tờ về quyền sử dụng đất, kèm theo bản trích lục bản đồ đất hoặc trích đo trên thực địa xác định sơ đồ ranh giới lô đất, cao độ và tỉ lệ đúng quy định địa chính.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Giấy phép đăng ký kinh doanh (nếu là công trình xây dựng của doanh nghiệp).</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Hồ sơ thiết kế của Công ty Thiết kế có pháp nhân gồm: Mặt bằng công trình trên lô đất tỷ lệ 1/200-1/500, sơ đồ vị trí công trình, mặt bằng các tầng, mặt đứng và mặt cắt công trình tỷ lệ 1/100, mặt bằng móng, chi tiết mặt cắt móng, sơ đồ cấp thoát nước, điện.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">Xin gia hạn giấy phép xây dựng, thời gian gia hạn bao lâu?</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">Trong thời hạn 12 tháng kể từ khi có giấy phép xây dựng mà công trình chưa khởi công thì người xin cấp phép phải xin gia hạn, Giấy phép xây dựng có thể gia hạn được nhiều lần, mỗi lần gia hạn là 12 tháng.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">Hồ sơ xin gia hạn gồm: Đơn xin gia hạn giấy phép xây dựng (theo mẫu), bản chính Giấy phép xây dựng đã được cấp. Thời gian giải quyết hồ sơ gia hạn giấy phép xây dựng không quá năm ngày làm việc.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">Thủ tục hoàn công được quy định như thế nào?</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">Sau khi xây dựng xong, chủ nhà phải nộp hồ sơ hoàn công.Cơ quan cấp phép cũng chính là cơ quan ra biên bản hoàn công. Hồ sơ hoàn công bao gồm:</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">• Giấy báo đề nghị kiểm tra công trình hoàn thành (theo mẫu).<br />\r\n• Bản sao giấy phép xây dựng.<br />\r\n• Bản sao hợp đồng thi công với nhà thầu xây dựng có tư cách pháp nhân (có thị thực).</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">Xin phép sửa chữa, cải tạo, mở rộng nhà ở riêng lẻ của tư nhân.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">Hồ sơ xin cấp giấy phép sửa chữa được lập thành 03 bộ và nộp tại UBND Quận/Huyện gồm:</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Đơn xin cấp giấy phép xây dựng (theo mẫu) do chủ nhà đứng tên.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Bản sao có thị thực một trong những giấy tờ về quyền sử dụng đất và quyền sở hữu nhà (nếu có) kèm theo bản trích lục bản đồ đất hoặc trích đo trên thực địa xác định sơ đồ ranh giới lô đất, cao độ và tỉ lệ đúng quy định địa chính.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Giấy phép đăng ký kinh doanh (nếu là công trình xây dựng của doanh nghiệp).</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Hồ sơ thiết kế gồm:</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Mặt bằng công trình trên lô đất tỷ lệ 1/200-1/500, sơ đồ vị trí công trình, mặt bằng các tầng, mặt đứng và mặt cắt công trình tỷ lệ 1/100, mặt bằng móng, chi tiết mặt cắt móng, sơ đồ cấp thoát nước, điện.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Ảnh chụp mặt chính căn nhà xin sửa chữa và hai căn liên kế hai bên (khổ 9x12).</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Hồ sơ khảo sát hiện trạng móng (xác định khả năng nâng tầng và biện pháp gia cố) của tổ chức tư vấn có pháp nhân (trường hợp có nâng tầng).</span></p>', 0, 2, 'nhung-luu-y-khi-xay-dung-nha-cap-4', '2021-08-20 12:25:50', '2021-08-20 12:25:50'),
(12, 'article_cam-nang-xay-dung-nha-o_1629487617222.jpg', 'CẨM NANG XÂY DỰNG NHÀ Ở', 'CẨM NANG XÂY DỰNG NHÀ Ở', '', '', '', '<h2 style=\"text-align:justify\"><span style=\"font-size:14px\"><strong>Những lưu ý đầu tiên khi chuẩn bị xây dựng nhà:</strong></span></h2>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\"><span style=\"font-size:14px\">Đầu tiên bạn cần biết được đâu là nhu cầu cơ bản nhất của ngôi nhà bạn sắp sửa xây dựng, cụ thể như một số các yêu cầu: số lượng phòng, diện tích và vị trí các phòng, phong cách và vật dụng trang trí nội thất, không gian dự trữ, phòng thờ, phòng kho, sân phơi, bồn chứa nước.</span></li>\r\n	<li style=\"text-align:justify\"><span style=\"font-size:14px\">Cần lưu ý thêm về các thay đổi trong tương lai nhà bạn, ví dụ như có thêm thành viên mới, cần thêm phòng, mua oto chẳng hạn</span></li>\r\n	<li style=\"text-align:justify\"><span style=\"font-size:14px\">Nên thảm khảo thêm các ý kiến trong gia đình khi thông qua kế hoạch lần cuối.</span></li>\r\n	<li style=\"text-align:justify\"><span style=\"font-size:14px\">Sắp xếp các thông tin ở trên để tạo ra thứ tự các việc ưu tiên.</span></li>\r\n	<li style=\"text-align:justify\"><span style=\"font-size:14px\">Tập hợp các thông tin để khi làm việc với kiến trúc sư bạn có được kế hoạch làm việc tốt và hoàn hảo.</span></li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\"><strong>Để giúp mọi người có cái nhìn tổng thể và giải quyết được những khó khăn ấy, chúng tôi xin liệt kê chi tiết các bước từ khi chuẩn bị đến khi hoàn thiện một ngôi nhà&nbsp; như sau:</strong></span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\"><em>Bước 1: Lập kế hoạch xây nhà</em><br />\r\n<em>Bước 2: Chọn nhà tư vấn thiết kế xây dựng</em><br />\r\n<em>Bước 3: Hoàn thiện hồ sơ xây dựng, lựa chọn nhà thầu xây dựng</em><br />\r\n<em>Bước 4: Các thủ chuẩn bị khởi công</em><br />\r\n<em>Bước 5: Chuẩn bị mặt bằng, thi công phần nền móng</em><br />\r\n<em>Bước 6: Thi công phần khung nhà (phần thô)</em><br />\r\n<em>Bước 7: Thi công hoàn thiện nhà</em><br />\r\n<em>Bước 8: Mua sắm, lắp đặt nội thất</em></span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">Cụ thể từng bước được liệt kê ở dưới đây.</span></p>\r\n\r\n<h3 style=\"text-align:justify\"><span style=\"font-size:14px\">Bước 1: Lập kế hoạch xây nhà</span></h3>\r\n\r\n<div style=\"font-size: 16px; color: rgb(63, 63, 63); font-family: muli;\">\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\"><strong>1. Kế hoạch về tài chính;</strong></span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">Vấn đề rất quan trọng nhất trước khi bạn định xây nhà là chính là Tiền ($) để xây nhà, nếu bạn xem nhẹ việc lập kế hoạch chi tiêu cho xây nhà, có thể bạn sẽ gặp khó khăn lớn khi đối diện với phát sinh hoặc nó có thể ảnh hưởng tới tài chính hiện tại của gia đình bạn. Không nên để trường hợp bạn bị cạn kiện tiền khi công trình đang xây dựng dở dang, và dự trù kinh phí xây dựng nhà trước bạn cũng có thể tránh được việc vắt kiệt sức để trả nợ sau khi xây xong nhà. Cách tốt nhất là đầu tiên bạn nên dự trù trước&nbsp;<strong>chi phí xây dựng nhà</strong>, thông thường có 2 loại chi phí chính cần ước tính:<br />\r\n<br />\r\n<em>a/ Ước tính chi phí xây dựng cơ bản:</em></span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">Đây là chi phí bạn cần để xây dựng ngôi nhà đến mức độ hoàn thiện phần kiên cố và có thể đã bao gồm phần gạch lát trang trí, trần thạch cao, kệ bếp và sơn nước trong ngoài.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\"><strong>Chi phí cơ bản loại này gồm</strong>:&nbsp;<em>Chi phí tư vấn thiết kế (để có được bản vẽ kỹ thuật thi công)</em>&nbsp;+&nbsp;<em>Chi phí thi công xây dựng</em>&nbsp;+&nbsp;<em>Chi phí giám sát (hoặc chủ nhà tự giám sát)</em>.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">Về&nbsp;<strong>chi phí thi công xây dựng</strong>&nbsp;là chi phí lớn nhất, cách tính phổ biến hiện nay là các công ty xây dựng cũng như đơn vị thi công thường lấy:&nbsp; (<em>m2 mặt sàn xây dựng)</em>&nbsp;<strong>x</strong>&nbsp;<em>(đơn giá 1m2)</em>, cách tính này chỉ tương đối, cách tính chính xác nhất là bạn nên yêu cầu đơn vị tư vấn thiết kế lập đơn giá theo dự toán chi tiết các hạng mục. việc tính chi tiết từng hạng mục sẽ cho bạn cái nhìn rõ hơn và chi tiết hơn về các chi phí từng hạng mục cho ngôi nhà bạn, ví dụ như xây dựng phần cầu thang chi phí hết bao nhiêu, phần mái, tường giá thế nào, như vậy chi phí tài chính sẽ càng rõ ràng hơn cả.<br />\r\n<br />\r\n<em>b/&nbsp; Ước tính chi phí phát sinh:</em></span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">Thực tế khi xây nhà luôn có chi phí phát sinh, vì vậy ngoài số tiền chi phí xây dựng cơ bản bạn nên dự trù 10 -30% số tiền gọi là dự phòng phí, với khoản dự phòng đó bạn có thể yên tâm hơn khi trao đổi nhu cầu của mình với kiến trúc sư và nhà thầu thi công.<br />\r\n<br />\r\n<em>c/ Ước tính chi phí trang trí nội thất:</em></span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">Chi phí trang trí nội thất là các chi phí cho việc mua đồ dùng trong gia đình, hoặc trang trí thẩm mỹ thêm cho ngôi nhà bạn. Bạn có thể tính chi phí này bao gồm chi phí để mua thiết bị nhà tắm, bếp ga, bếp điện, máy lạnh, bàn ghế, đèn trang trí, và các thiết bị gia dụng khác,… Lý do chúng tôi khuyên bạn nên tách riêng chi phí này vì đây là phần dời&nbsp; và hoàn toàn có thể được trang bị sau khi ngôi nhà hoàn thành. Thời gian trang bị thêm những đồ này không phụ thuộc vào thời gian xây nhà.</span></p>\r\n</div>', 0, 2, 'cam-nang-xay-dung-nha-o', '2021-08-20 12:26:57', '2021-08-20 12:26:57');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `thumbnail` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `created_at`, `updated_at`, `thumbnail`) VALUES
(1, '2021-08-20 01:07:53', '2021-08-20 01:07:53', 'banner_1629446872582.jpg'),
(2, '2021-08-20 01:08:04', '2021-08-20 01:08:04', 'banner_1629446884564.jpg'),
(3, '2021-08-20 01:08:12', '2021-08-20 01:08:12', 'banner_1629446892484.jpg'),
(4, '2021-08-20 01:08:20', '2021-08-20 01:08:20', 'banner_1629446899789.jpg'),
(5, '2021-08-20 01:08:28', '2021-08-20 01:08:28', 'banner_1629446908117.jpg'),
(6, '2021-08-20 01:08:35', '2021-08-20 01:08:35', 'banner_1629446914559.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyword` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`, `seo_title`, `keyword`, `key_description`) VALUES
(1, 'Xi măng', '2021-08-20 01:21:36', '2021-08-31 03:15:10', 'xi-mang', NULL, NULL),
(2, 'Gạch', '2021-08-20 01:21:44', '2021-08-31 03:15:19', 'gach', NULL, NULL),
(3, 'abc', '2021-08-20 20:45:14', '2021-08-31 03:15:27', 'abc', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_company` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_sale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `google_map` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zalo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc_video_projects` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc_info` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `favicon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name_company`, `address`, `phone`, `phone_sale`, `email`, `name_contact`, `phone_contact`, `google_map`, `youtube`, `zalo`, `title`, `desc`, `keywords`, `desc_video_projects`, `desc_info`, `favicon`, `created_at`, `updated_at`) VALUES
(1, 'Tên công ty', 'Địa chỉ công ty', '01234567890', '01234567890', 'admin@gmail.com', 'Tên người liên hệ', '01234567890', '#', '#', '01234567890', 'Tên trang web', 'Mô tả trang web', 'thiết kế, nội thất, nhà đất', 'Các Video dự án được Công ty DVT đầu tư tư chuyên nghiệp và tỉ mỉ để Quý khách hàng có cái nhìn tổng quan hơn về từng dự án.', 'Ngay từ ngày đầu thành lập, Công ty DVT đã thực hiện các dịch vụ thiết kế và xây dựng trên nhiều địa phương khắp cả nước. Trên suốt chặng đường qua, Công ty DVT đã luôn tạo được dấu ấn tốt cho các khách hàng, không chỉ về mặt chuyên môn, mà còn gắn kết với khách hàng trong từng dự án, đảm bảo sự hài lòng và đạt được sự tín nhiệm lâu dài.', '', '2021-08-16 09:40:05', '2021-08-16 09:40:05');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `medias`
--

CREATE TABLE `medias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image_src` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `imageable_id` int(11) NOT NULL,
  `imageable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `medias`
--

INSERT INTO `medias` (`id`, `image_src`, `imageable_id`, `imageable_type`, `created_at`, `updated_at`) VALUES
(1, 'product_xi-mang-xay-dung-ban-le-buon-1kg_1629447799777.jpg', 1, 'App\\Models\\Product', '2021-08-20 01:23:20', '2021-08-20 01:23:20'),
(2, 'product_xi-mang-xay-dung-ban-le-buon-1kg_1629447800095.jpg', 1, 'App\\Models\\Product', '2021-08-20 01:23:20', '2021-08-20 01:23:20'),
(3, 'product_xi-mang-xay-dung-ban-le-buon-1kg_1629447800216.jpg', 1, 'App\\Models\\Product', '2021-08-20 01:23:20', '2021-08-20 01:23:20'),
(4, 'product_xi-mang-2_1629447887266.jpg', 2, 'App\\Models\\Product', '2021-08-20 01:24:47', '2021-08-20 01:24:47'),
(5, 'product_xi-mang-2_1629447887453.jpg', 2, 'App\\Models\\Product', '2021-08-20 01:24:47', '2021-08-20 01:24:47'),
(6, 'product_xi-mang-2_1629447887647.jpg', 2, 'App\\Models\\Product', '2021-08-20 01:24:47', '2021-08-20 01:24:47'),
(7, 'product_gach-ve-tranh-3d-ca-vang_1629448074175.jpg', 3, 'App\\Models\\Product', '2021-08-20 01:27:54', '2021-08-20 01:27:54'),
(8, 'product_gach-ve-tranh-3d-ca-vang_1629448074232.jpg', 3, 'App\\Models\\Product', '2021-08-20 01:27:54', '2021-08-20 01:27:54'),
(9, 'product_gach-ve-tranh-3d-ca-vang_1629448074282.jpg', 3, 'App\\Models\\Product', '2021-08-20 01:27:54', '2021-08-20 01:27:54'),
(10, 'product_gach-tranh-2d-ca-chep_1629448256729.jpg', 4, 'App\\Models\\Product', '2021-08-20 01:30:56', '2021-08-20 01:30:56'),
(11, 'product_gach-tranh-2d-ca-chep_1629448256820.jpg', 4, 'App\\Models\\Product', '2021-08-20 01:30:56', '2021-08-20 01:30:56'),
(12, 'product_gach-tranh-2d-ca-chep_1629448256866.jpg', 4, 'App\\Models\\Product', '2021-08-20 01:30:56', '2021-08-20 01:30:56'),
(13, 'project_biet-thu-mai-thai-hien-dai-anh-hung-phan-thiet-binh-thuan_1629487864865.jpg', 1, 'App\\Models\\Project', '2021-08-20 12:31:05', '2021-08-20 12:31:05'),
(14, 'project_biet-thu-mai-thai-hien-dai-anh-hung-phan-thiet-binh-thuan_1629487865319.jpg', 1, 'App\\Models\\Project', '2021-08-20 12:31:05', '2021-08-20 12:31:05'),
(15, 'project_biet-thu-mai-thai-hien-dai-anh-hung-phan-thiet-binh-thuan_1629487865476.jpg', 1, 'App\\Models\\Project', '2021-08-20 12:31:05', '2021-08-20 12:31:05'),
(16, 'project_biet-thu-mai-thai-hien-dai-anh-hung-phan-thiet-binh-thuan_1629487865631.jpg', 1, 'App\\Models\\Project', '2021-08-20 12:31:05', '2021-08-20 12:31:05'),
(17, 'project_nha-anh-thuan_1629487991235.jpg', 2, 'App\\Models\\Project', '2021-08-20 12:33:11', '2021-08-20 12:33:11'),
(18, 'project_nha-anh-thuan_1629487991528.jpg', 2, 'App\\Models\\Project', '2021-08-20 12:33:11', '2021-08-20 12:33:11'),
(19, 'project_nha-anh-thuan_1629487991741.jpg', 2, 'App\\Models\\Project', '2021-08-20 12:33:12', '2021-08-20 12:33:12'),
(20, 'project_nha-anh-thuan_1629487992056.jpg', 2, 'App\\Models\\Project', '2021-08-20 12:33:12', '2021-08-20 12:33:12'),
(21, 'project_nha-anh-thuan_1629487992326.jpg', 2, 'App\\Models\\Project', '2021-08-20 12:33:12', '2021-08-20 12:33:12'),
(22, 'project_biet-thu-hien-dai-chi-thu-hoa-thang-binh-thuan_1629488220045.jpg', 3, 'App\\Models\\Project', '2021-08-20 12:37:00', '2021-08-20 12:37:00'),
(23, 'project_biet-thu-hien-dai-chi-thu-hoa-thang-binh-thuan_1629488220293.jpg', 3, 'App\\Models\\Project', '2021-08-20 12:37:00', '2021-08-20 12:37:00'),
(24, 'project_biet-thu-hien-dai-chi-thu-hoa-thang-binh-thuan_1629488220464.jpg', 3, 'App\\Models\\Project', '2021-08-20 12:37:00', '2021-08-20 12:37:00'),
(25, 'project_biet-thu-hien-dai-chi-thu-hoa-thang-binh-thuan_1629488220680.jpg', 3, 'App\\Models\\Project', '2021-08-20 12:37:00', '2021-08-20 12:37:00'),
(26, 'project_biet-thu-hien-dai-chi-thu-hoa-thang-binh-thuan_1629488220862.jpg', 3, 'App\\Models\\Project', '2021-08-20 12:37:01', '2021-08-20 12:37:01'),
(27, 'project_biet-thu-hien-dai-chi-thu-hoa-thang-binh-thuan_1629488221047.jpg', 3, 'App\\Models\\Project', '2021-08-20 12:37:01', '2021-08-20 12:37:01'),
(28, 'project_biet-thu-hien-dai-chi-thu-hoa-thang-binh-thuan_1629488221233.jpg', 3, 'App\\Models\\Project', '2021-08-20 12:37:01', '2021-08-20 12:37:01'),
(29, 'project_biet-thu-hien-dai-chi-thu-hoa-thang-binh-thuan_1629488221434.jpg', 3, 'App\\Models\\Project', '2021-08-20 12:37:01', '2021-08-20 12:37:01'),
(30, 'project_biet-thu-hien-dai-chi-thu-hoa-thang-binh-thuan_1629488221692.jpg', 3, 'App\\Models\\Project', '2021-08-20 12:37:01', '2021-08-20 12:37:01'),
(31, 'project_biet-thu-hien-dai-chi-thu-hoa-thang-binh-thuan_1629488221943.jpg', 3, 'App\\Models\\Project', '2021-08-20 12:37:02', '2021-08-20 12:37:02'),
(32, 'project_biet-thu-hien-dai-chi-thu-hoa-thang-binh-thuan_1629488222170.jpg', 3, 'App\\Models\\Project', '2021-08-20 12:37:02', '2021-08-20 12:37:02'),
(33, 'project_biet-thu-chu-chanh-can-cu-6_1629488484354.jpg', 4, 'App\\Models\\Project', '2021-08-20 12:41:24', '2021-08-20 12:41:24'),
(34, 'project_biet-thu-chu-chanh-can-cu-6_1629488484558.jpg', 4, 'App\\Models\\Project', '2021-08-20 12:41:24', '2021-08-20 12:41:24'),
(35, 'project_biet-thu-chu-chanh-can-cu-6_1629488484767.jpg', 4, 'App\\Models\\Project', '2021-08-20 12:41:24', '2021-08-20 12:41:24'),
(36, 'project_biet-thu-chu-chanh-can-cu-6_1629488484956.jpg', 4, 'App\\Models\\Project', '2021-08-20 12:41:25', '2021-08-20 12:41:25'),
(37, 'project_biet-thu-chu-chanh-can-cu-6_1629488485134.jpg', 4, 'App\\Models\\Project', '2021-08-20 12:41:25', '2021-08-20 12:41:25'),
(38, 'project_khach-san-anh-son-phan-thiet_1629488567393.jpg', 5, 'App\\Models\\Project', '2021-08-20 12:42:47', '2021-08-20 12:42:47'),
(39, 'project_khach-san-anh-son-phan-thiet_1629488567668.jpg', 5, 'App\\Models\\Project', '2021-08-20 12:42:47', '2021-08-20 12:42:47'),
(40, 'project_khach-san-anh-son-phan-thiet_1629488567905.jpg', 5, 'App\\Models\\Project', '2021-08-20 12:42:48', '2021-08-20 12:42:48'),
(41, 'project_khach-san-anh-son-phan-thiet_1629488568179.jpg', 5, 'App\\Models\\Project', '2021-08-20 12:42:48', '2021-08-20 12:42:48'),
(42, 'project_khach-san-anh-son-phan-thiet_1629488568421.jpg', 5, 'App\\Models\\Project', '2021-08-20 12:42:48', '2021-08-20 12:42:48'),
(43, 'project_biet-thu-anh-phong-phan-ri_1629488662329.jpg', 6, 'App\\Models\\Project', '2021-08-20 12:44:22', '2021-08-20 12:44:22'),
(44, 'project_biet-thu-anh-phong-phan-ri_1629488662645.jpg', 6, 'App\\Models\\Project', '2021-08-20 12:44:22', '2021-08-20 12:44:22'),
(45, 'project_biet-thu-anh-phong-phan-ri_1629488662989.jpg', 6, 'App\\Models\\Project', '2021-08-20 12:44:23', '2021-08-20 12:44:23'),
(46, 'project_biet-thu-anh-phong-phan-ri_1629488663168.jpg', 6, 'App\\Models\\Project', '2021-08-20 12:44:23', '2021-08-20 12:44:23'),
(47, 'project_biet-thu-anh-phong-phan-ri_1629488663375.jpg', 6, 'App\\Models\\Project', '2021-08-20 12:44:23', '2021-08-20 12:44:23'),
(48, 'project_biet-thu-tret-anh-huynh-tan-muong-man-binh-thuan_1629488746331.jpg', 7, 'App\\Models\\Project', '2021-08-20 12:45:46', '2021-08-20 12:45:46'),
(49, 'project_biet-thu-tret-anh-huynh-tan-muong-man-binh-thuan_1629488746507.jpg', 7, 'App\\Models\\Project', '2021-08-20 12:45:46', '2021-08-20 12:45:46'),
(50, 'project_biet-thu-tret-anh-huynh-tan-muong-man-binh-thuan_1629488746786.jpg', 7, 'App\\Models\\Project', '2021-08-20 12:45:46', '2021-08-20 12:45:46'),
(51, 'project_biet-thu-tret-anh-huynh-tan-muong-man-binh-thuan_1629488746998.jpg', 7, 'App\\Models\\Project', '2021-08-20 12:45:47', '2021-08-20 12:45:47'),
(52, 'project_biet-thu-tret-anh-huynh-tan-muong-man-binh-thuan_1629488747248.jpg', 7, 'App\\Models\\Project', '2021-08-20 12:45:47', '2021-08-20 12:45:47'),
(53, 'project_biet-thu-tret-anh-trung-phan-thiet_1629488818487.jpg', 8, 'App\\Models\\Project', '2021-08-20 12:46:58', '2021-08-20 12:46:58'),
(54, 'project_biet-thu-tret-anh-trung-phan-thiet_1629488818648.jpg', 8, 'App\\Models\\Project', '2021-08-20 12:46:58', '2021-08-20 12:46:58'),
(55, 'project_biet-thu-tret-anh-trung-phan-thiet_1629488818940.jpg', 8, 'App\\Models\\Project', '2021-08-20 12:46:59', '2021-08-20 12:46:59'),
(56, 'project_biet-thu-tret-anh-trung-phan-thiet_1629488819082.jpg', 8, 'App\\Models\\Project', '2021-08-20 12:46:59', '2021-08-20 12:46:59'),
(57, 'project_biet-thu-tret-anh-trung-phan-thiet_1629488819267.jpg', 8, 'App\\Models\\Project', '2021-08-20 12:46:59', '2021-08-20 12:46:59'),
(58, 'project_biet-thu-tret-chi-kieu_1629488882596.jpg', 9, 'App\\Models\\Project', '2021-08-20 12:48:02', '2021-08-20 12:48:02'),
(59, 'project_biet-thu-tret-chi-kieu_1629488882774.jpg', 9, 'App\\Models\\Project', '2021-08-20 12:48:03', '2021-08-20 12:48:03'),
(60, 'project_biet-thu-tret-chi-kieu_1629488883094.jpg', 9, 'App\\Models\\Project', '2021-08-20 12:48:03', '2021-08-20 12:48:03'),
(61, 'project_biet-thu-tret-chi-kieu_1629488883361.jpg', 9, 'App\\Models\\Project', '2021-08-20 12:48:03', '2021-08-20 12:48:03'),
(62, 'project_biet-thu-tret-chi-kieu_1629488883612.jpg', 9, 'App\\Models\\Project', '2021-08-20 12:48:03', '2021-08-20 12:48:03'),
(63, 'project_nha-cong-nhung-ktdc-dong-xuan-an_1629488964109.jpg', 10, 'App\\Models\\Project', '2021-08-20 12:49:24', '2021-08-20 12:49:24'),
(64, 'project_nha-co-hong-lam-dinh-truc-phoi-canh_1629489044161.jpg', 11, 'App\\Models\\Project', '2021-08-20 12:50:44', '2021-08-20 12:50:44'),
(65, 'project_biet-thu-nha-chi-diem-hoang-bich-son_1629489125491.jpg', 12, 'App\\Models\\Project', '2021-08-20 12:52:05', '2021-08-20 12:52:05'),
(66, 'project_nha-pho-hien-dai-anh-minh-truong-binh-thuan_1629489189177.jpg', 13, 'App\\Models\\Project', '2021-08-20 12:53:09', '2021-08-20 12:53:09'),
(67, 'project_nha-pho-hien-dai-chi-tham-binh-thuan_1629489248457.jpg', 14, 'App\\Models\\Project', '2021-08-20 12:54:08', '2021-08-20 12:54:08'),
(68, 'project_nha-pho-hien-dai-chi-tham-binh-thuan_1629489248822.jpg', 14, 'App\\Models\\Project', '2021-08-20 12:54:09', '2021-08-20 12:54:09'),
(69, 'project_nha-pho-hien-dai-chi-tham-binh-thuan_1629489249166.jpg', 14, 'App\\Models\\Project', '2021-08-20 12:54:09', '2021-08-20 12:54:09'),
(70, 'project_biet-thu-tret-anh-sang-phan-thiet_1629489336108.jpg', 15, 'App\\Models\\Project', '2021-08-20 12:55:36', '2021-08-20 12:55:36'),
(71, 'project_biet-thu-tret-anh-sang-phan-thiet_1629489336234.jpg', 15, 'App\\Models\\Project', '2021-08-20 12:55:36', '2021-08-20 12:55:36'),
(72, 'project_biet-thu-tret-anh-sang-phan-thiet_1629489336636.jpg', 15, 'App\\Models\\Project', '2021-08-20 12:55:37', '2021-08-20 12:55:37'),
(73, 'project_biet-thu-tret-anh-sang-phan-thiet_1629489337060.jpg', 15, 'App\\Models\\Project', '2021-08-20 12:55:37', '2021-08-20 12:55:37'),
(74, 'project_biet-thu-tret-anh-sang-phan-thiet_1629489337457.jpg', 15, 'App\\Models\\Project', '2021-08-20 12:55:37', '2021-08-20 12:55:37'),
(75, 'project_biet-thu-anh-phong-phan-ri_1629489389179.jpg', 16, 'App\\Models\\Project', '2021-08-20 12:56:29', '2021-08-20 12:56:29'),
(76, 'project_biet-thu-anh-phong-phan-ri_1629489389599.jpg', 16, 'App\\Models\\Project', '2021-08-20 12:56:30', '2021-08-20 12:56:30'),
(77, 'project_biet-thu-anh-phong-phan-ri_1629489390090.jpg', 16, 'App\\Models\\Project', '2021-08-20 12:56:30', '2021-08-20 12:56:30'),
(78, 'project_nha-pho-hien-dai-chi-nau_1629489471115.jpg', 17, 'App\\Models\\Project', '2021-08-20 12:57:51', '2021-08-20 12:57:51'),
(79, 'project_nha-pho-hien-dai-chi-nau_1629489471373.jpg', 17, 'App\\Models\\Project', '2021-08-20 12:57:51', '2021-08-20 12:57:51'),
(80, 'project_nha-pho-hien-dai-chi-nau_1629489471715.jpg', 17, 'App\\Models\\Project', '2021-08-20 12:57:52', '2021-08-20 12:57:52'),
(81, 'project_nha-pho-hien-dai-chi-nau_1629489472295.jpg', 17, 'App\\Models\\Project', '2021-08-20 12:57:52', '2021-08-20 12:57:52'),
(82, 'project_nha-pho-hien-dai-chi-nau_1629489472672.jpg', 17, 'App\\Models\\Project', '2021-08-20 12:57:53', '2021-08-20 12:57:53'),
(83, 'project_nha-cap-4-hien-dai-chi-phuong-phan-thiet_1629489535051.jpg', 18, 'App\\Models\\Project', '2021-08-20 12:58:55', '2021-08-20 12:58:55'),
(84, 'project_nha-cap-4-hien-dai-anh-quy-chi-trinh-ham-thang_1629489592546.jpg', 19, 'App\\Models\\Project', '2021-08-20 12:59:52', '2021-08-20 12:59:52'),
(85, 'project_biet-thu-anh-luan_1629489724565.jpg', 20, 'App\\Models\\Project', '2021-08-20 13:02:04', '2021-08-20 13:02:04'),
(86, 'project_biet-thu-anh-luan_1629489724915.jpg', 20, 'App\\Models\\Project', '2021-08-20 13:02:05', '2021-08-20 13:02:05'),
(87, 'project_biet-thu-anh-luan_1629489725202.jpg', 20, 'App\\Models\\Project', '2021-08-20 13:02:05', '2021-08-20 13:02:05'),
(88, 'project_biet-thu-anh-luan_1629489725431.jpg', 20, 'App\\Models\\Project', '2021-08-20 13:02:05', '2021-08-20 13:02:05'),
(89, 'project_biet-thu-anh-luan_1629489725696.jpg', 20, 'App\\Models\\Project', '2021-08-20 13:02:05', '2021-08-20 13:02:05'),
(90, 'project_bcd_1630383926675.jpg', 22, 'App\\Models\\Project', '2021-08-30 21:25:27', '2021-08-30 21:25:27'),
(91, 'project_abc_1630383966656.jpg', 21, 'App\\Models\\Project', '2021-08-30 21:26:06', '2021-08-30 21:26:06'),
(92, 'project_123_1630384050784.jpg', 23, 'App\\Models\\Project', '2021-08-30 21:27:30', '2021-08-30 21:27:30'),
(93, 'project_1234_1630384312071.jpg', 24, 'App\\Models\\Project', '2021-08-30 21:31:52', '2021-08-30 21:31:52'),
(94, 'project_abcde_1630390500242.jpg', 25, 'App\\Models\\Project', '2021-08-30 23:15:00', '2021-08-30 23:15:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(155, '2021_07_30_162948_add_default_value_view_column_to_articles_table', 1),
(166, '2014_10_12_000000_create_users_table', 2),
(167, '2014_10_12_100000_create_password_resets_table', 2),
(168, '2019_08_19_000000_create_failed_jobs_table', 2),
(169, '2021_07_28_103049_create_medias_table', 2),
(170, '2021_07_28_103223_create_categories_table', 2),
(171, '2021_07_28_103540_create_price_list_table', 2),
(172, '2021_07_28_103603_create_banners_table', 2),
(173, '2021_07_28_103701_create_news_table', 2),
(174, '2021_07_28_103835_create_arch_knowledge_table', 2),
(175, '2021_07_28_103910_create_products_table', 2),
(176, '2021_07_28_103925_create_articles_table', 2),
(177, '2021_07_29_171332_add_thumbnail_to_banners_table', 2),
(178, '2021_07_30_134938_modify_type_id_column_to_price_list_table', 2),
(179, '2021_07_30_144915_rename_type_id_column_to_price_list_table', 2),
(180, '2021_07_30_164652_add_view_column_to_products_table', 2),
(181, '2021_07_30_164902_add_view_column_to_price_list_table', 2),
(182, '2021_08_03_171222_drop_price_list_table', 2),
(183, '2021_08_03_171302_drop_arch_knowledge_table', 2),
(184, '2021_08_05_031730_drop_news_table', 2),
(185, '2021_08_05_032305_create_videos_table', 2),
(186, '2021_08_07_173905_drop_videos_table', 2),
(187, '2021_08_07_174401_create_projects_table', 2),
(188, '2021_08_09_192653_create_contact_table', 2),
(189, '2021_08_09_192723_create_youtube_table', 2),
(190, '2021_08_31_091436_add_seo_column_to_categories_table', 3),
(191, '2021_08_31_132542_modify_service_id_column_to_projects_table', 4),
(192, '2021_08_31_132558_rename_service_id_column_to_projects_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyword` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `view` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `seo_title`, `keyword`, `key_description`, `description`, `content`, `sku`, `price`, `slug`, `category_id`, `created_at`, `updated_at`, `view`) VALUES
(1, 'Xi măng xây dựng bán lẻ - buôn (1kg)', 'xm-1', '', '', '', '<p><span style=\"color:rgba(0, 0, 0, 0.8); font-family:helvetica neue,helvetica,arial,文泉驛正黑,wenquanyi zen hei,hiragino sans gb,儷黑 pro,lihei pro,heiti tc,微軟正黑體,microsoft jhenghei ui,microsoft jhenghei,sans-serif; font-size:14px\">Công dụng của xi măng thì ai cũng biết nhưng việc tìm mua không dễ dàng với đa số chúng ta. Bạn đang cần xi măng để dùng cho nhu cầu gia đình mà gần khu vực bạn sinh sống không có cửa hàng bán vật liệu xây dựng ? Bạn cần mua xi măng về phục vụ cho rất nhiều nhu cầu trong cuộc sống ? Bạn muốn mua xi măng mà chỉ cần một lượng nhỏ và ngại tới cửa hàng vật liệu xây dựng ? Đặc biệt, bạn mua vỏ tạ nhà Ngọc Nam Sport nhưng chưa biết mua xi măng ở đâu cho tiện.</span></p>', 'TCVN 6260-1997', 500000, 'xi-mang-xay-dung-ban-le-buon-1kg', 1, '2021-08-20 01:23:19', '2021-08-30 23:21:59', 8),
(2, 'Xi măng 2', 'xm-2', '', '', '', '<p><span style=\"color:rgba(0, 0, 0, 0.8); font-family:helvetica neue,helvetica,arial,文泉驛正黑,wenquanyi zen hei,hiragino sans gb,儷黑 pro,lihei pro,heiti tc,微軟正黑體,microsoft jhenghei ui,microsoft jhenghei,sans-serif; font-size:14px\">Công dụng của xi măng thì ai cũng biết nhưng việc tìm mua không dễ dàng với đa số chúng ta. Bạn đang cần xi măng để dùng cho nhu cầu gia đình mà gần khu vực bạn sinh sống không có cửa hàng bán vật liệu xây dựng ? Bạn cần mua xi măng về phục vụ cho rất nhiều nhu cầu trong cuộc sống ? Bạn muốn mua xi măng mà chỉ cần một lượng nhỏ và ngại tới cửa hàng vật liệu xây dựng ? Đặc biệt, bạn mua vỏ tạ nhà Ngọc Nam Sport nhưng chưa biết mua xi măng ở đâu cho tiện. Nay Ngọc Nam Sport có bán Xi Măng trên Shopee giúp mọi người đặt mua dễ dàng hơn. Về công dụng và cách dùng của xi măng cho phép Ngọc Nam không cần nói thêm vì ai ai cũng biết về loại sản phẩm quá thông dụng trong cuộc sống.</span></p>', 'xm-2', 200000, 'xi-mang-2', 1, '2021-08-20 01:24:47', '2021-08-20 01:33:53', 1),
(3, 'Gạch vẽ tranh 3D cá vàng', 'g-1', '', '', '', '<p style=\"text-align:justify\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', 'g-1', 150000, 'gach-ve-tranh-3d-ca-vang', 2, '2021-08-20 01:27:54', '2021-08-20 01:27:54', 0),
(4, 'Gạch tranh 2D cá chép', 'g-2', '', '', '', '<p style=\"text-align:justify\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', 'g-2', 200000, 'gach-tranh-2d-ca-chep', 2, '2021-08-20 01:30:56', '2021-08-30 23:23:02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyword` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view` int(11) NOT NULL DEFAULT 0,
  `type_id` int(11) NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` int(11) NOT NULL DEFAULT 0,
  `number_floor` int(11) NOT NULL DEFAULT 0,
  `fee` float DEFAULT NULL,
  `service` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year_complete` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `title`, `seo_title`, `keyword`, `key_description`, `description`, `content`, `view`, `type_id`, `slug`, `address`, `area`, `number_floor`, `fee`, `service`, `year_complete`, `created_at`, `updated_at`) VALUES
(1, 'Nhà cấp 4 anh Luân', 'Nhà cấp 4 anh Luân aaaaa', '', '', '', NULL, 124, 3, 'nha-cap-4-anh-luan', 'TP.HCM', 200, 2, 1000000000, NULL, 2030, '2021-08-20 12:31:04', '2021-08-31 07:08:10'),
(2, 'Nhà anh Thuận', 'Nhà anh Thuận', '', '', '', NULL, 563, 5, 'nha-anh-thuan', 'Hà Nội', 200, 3, 20000000, '', 2020, '2021-08-20 12:33:11', '2021-08-20 12:33:11'),
(3, 'Biệt thự hiện đại chị Thu - Hoà Thắng, Bình Thuận', 'Biệt thự hiện đại chị Thu - Hoà Thắng, Bình Thuận', '', '', '', NULL, 107, 0, 'biet-thu-hien-dai-chi-thu-hoa-thang-binh-thuan', 'Đà Nẵng', 300, 3, 200000000, '', 2030, '2021-08-20 12:37:00', '2021-08-31 07:11:18'),
(4, 'Biệt thự chú Chánh - Căn Cứ 6', 'Biệt thự chú Chánh - Căn Cứ 6', '', '', '', NULL, 231, 0, 'biet-thu-chu-chanh-can-cu-6', 'TP.HCM', 300, 3, 20000000, '', 2020, '2021-08-20 12:41:24', '2021-08-31 07:06:00'),
(5, 'Khách sạn anh Sơn - Phan Thiết', 'Khách sạn anh Sơn - Phan Thiết', '', '', '', NULL, 967, 0, 'khach-san-anh-son-phan-thiet', 'Phan Thiết', 300, 3, 100000000, '', 2030, '2021-08-20 12:42:47', '2021-08-20 12:42:47'),
(6, 'Biệt Thự Anh Phong - Phan Rí', 'Biệt Thự Anh Phong - Phan Rí', '', '', '', NULL, 325, 0, 'biet-thu-anh-phong-phan-ri', 'Hà Nội', 150, 3, 10000000, '', 2010, '2021-08-20 12:44:22', '2021-08-30 23:15:37'),
(7, 'Biệt Thự Trệt Anh Huỳnh Tân - Mương Mán, Bình Thuận', 'Biệt Thự Trệt Anh Huỳnh Tân - Mương Mán, Bình Thuận', '', '', '', NULL, 826, 1, 'biet-thu-tret-anh-huynh-tan-muong-man-binh-thuan', 'TP.HCM', 300, 3, 20000000, '', 2030, '2021-08-20 12:45:46', '2021-08-20 12:45:46'),
(8, 'Biệt thự trệt anh Trung - Phan Thiết', 'Biệt thự trệt anh Trung - Phan Thiết', '', '', '', NULL, 271, 1, 'biet-thu-tret-anh-trung-phan-thiet', 'TP.HCM', 300, 5, 150, '', 2030, '2021-08-20 12:46:58', '2021-08-20 12:46:58'),
(9, 'Biệt Thự Trệt -Chị Kiều', 'Biệt Thự Trệt -Chị Kiều', '', '', '', NULL, 174, 1, 'biet-thu-tret-chi-kieu', 'Hà Nội', 200, 1, 2000000000, '', 2030, '2021-08-20 12:48:02', '2021-08-20 12:49:39'),
(10, 'Nhà công Nhung - KTDC Đông Xuân An', 'Nhà công Nhung - KTDC Đông Xuân An', '', '', '', NULL, 173, 4, 'nha-cong-nhung-ktdc-dong-xuan-an', 'Hà Nội', 200, 10, 2000000000, '', 2030, '2021-08-20 12:49:24', '2021-08-20 12:49:29'),
(11, 'Nhà cô Hồng - Lâm Đình Trúc - Phối cảnh', 'Nhà cô Hồng - Lâm Đình Trúc - Phối cảnh', '', '', '', NULL, 813, 4, 'nha-co-hong-lam-dinh-truc-phoi-canh', 'TP.HCM', 300, 10, 200000000, '', 2030, '2021-08-20 12:50:44', '2021-08-20 12:50:44'),
(12, 'Biệt thự Nhà chị Diễm Hoàng Bích Sơn', 'Biệt thự Nhà chị Diễm Hoàng Bích Sơn', '', '', '', NULL, 829, 4, 'biet-thu-nha-chi-diem-hoang-bich-son', 'TP.HCM', 300, 2, 10000000000, '', 2030, '2021-08-20 12:52:05', '2021-08-20 12:52:05'),
(13, 'Nhà phố hiện đại anh Minh Trường - Bình Thuận', 'Nhà phố hiện đại anh Minh Trường - Bình Thuận', '', '', '', NULL, 284, 2, 'nha-pho-hien-dai-anh-minh-truong-binh-thuan', 'TP.HCM', 300, 2, 10000000000, '', 2030, '2021-08-20 12:53:09', '2021-08-20 12:53:09'),
(14, 'Nhà phố hiện đại chị Thắm - Bình Thuận', 'Nhà phố hiện đại chị Thắm - Bình Thuận', '', '', '', NULL, 274, 2, 'nha-pho-hien-dai-chi-tham-binh-thuan', 'Hà Nội', 300, 2, 20000000000, '', 2030, '2021-08-20 12:54:08', '2021-08-20 12:54:08'),
(15, 'Biệt Thự Trệt Anh Sáng - Phan Thiết', 'Biệt Thự Trệt Anh Sáng - Phan Thiết', '', '', '', NULL, 936, 1, 'biet-thu-tret-anh-sang-phan-thiet', 'Hà Nội', 30, 2, 1000000000, '', 2030, '2021-08-20 12:55:36', '2021-08-20 12:55:36'),
(16, 'Biệt Thự Anh Phong - Phan Rí', 'Biệt Thự Anh Phong - Phan Rí', '', '', '', NULL, 364, 1, 'biet-thu-anh-phong-phan-ri', 'Đà Nẵng', 300, 2, 100000000000, '', 2030, '2021-08-20 12:56:29', '2021-08-20 12:56:29'),
(17, 'Nhà phố hiện đại - Chị Nâu', 'Nhà phố hiện đại - Chị Nâu', '', '', '', NULL, 916, 2, 'nha-pho-hien-dai-chi-nau', 'TP.HCM', 200, 3, 1e16, '', 2030, '2021-08-20 12:57:51', '2021-08-20 12:57:51'),
(18, 'Nhà cấp 4 hiện đại chị Phương - Phan Thiết', 'Nhà cấp 4 hiện đại chị Phương - Phan Thiết', '', '', '', NULL, 163, 3, 'nha-cap-4-hien-dai-chi-phuong-phan-thiet', 'Hà Nội', 300, 2, 1000000000, '', 2030, '2021-08-20 12:58:55', '2021-08-20 12:58:55'),
(19, 'Nhà cấp 4 hiện đại anh Quý chị Trinh - Hàm Thắng', 'Nhà cấp 4 hiện đại anh Quý chị Trinh - Hàm Thắng', '', '', '', NULL, 967, 3, 'nha-cap-4-hien-dai-anh-quy-chi-trinh-ham-thang', 'Phan Thiết', 300, 3, 200000000000, '', 2030, '2021-08-20 12:59:52', '2021-08-20 12:59:52'),
(20, 'Biệt thự anh Luân', 'Biệt thự anh Luân', '', '', '', NULL, 4, 0, 'biet-thu-anh-luan', 'TP.HCM', 300, 2, 10000000000000, '', 2030, '2021-08-20 13:02:04', '2021-08-20 21:34:46'),
(21, 'abc', 'abc', '', '', '', NULL, 100, 0, 'abc', 'abc', 100, 3, 10000, '', 2000, '2021-08-30 21:22:05', '2021-08-30 21:22:05'),
(22, 'bcd', 'bcd', '', '', '', NULL, 0, 0, 'bcd', 'bcd', 200, 3, 1000000, '', 2000, '2021-08-30 21:25:26', '2021-08-30 21:25:26'),
(23, '123', '123', '', '', '', NULL, 0, 0, '123', '123', 123, 1, 123, '', 2000, '2021-08-30 21:27:30', '2021-08-30 21:27:30'),
(24, '1234', '1234', '', '', '', NULL, 0, 0, '1234', '1234', 1234, 12, 1234, '', 2000, '2021-08-30 21:31:52', '2021-08-30 21:31:52'),
(25, 'abcde', 'abcde', '', '', '', NULL, 0, 0, 'abcde', 'abcde', 100, 3, 30000, '', 2000, '2021-08-30 23:15:00', '2021-08-30 23:15:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '2021-08-19 16:46:59', '$2y$10$KKmL0kixhAXu4M8PS4ZQzuBWzx33vp5cytK0KVmImlDW8C8qd22Ce', 'LeblLfx4Db', '2021-08-19 16:46:59', '2021-08-19 16:46:59');

-- --------------------------------------------------------

--
-- Table structure for table `youtube`
--

CREATE TABLE `youtube` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `youtube`
--

INSERT INTO `youtube` (`id`, `link`, `title`, `created_at`, `updated_at`) VALUES
(1, 'https://youtu.be/eAlrkO2glsQ', 'video 1', '2021-08-20 12:19:55', '2021-08-20 12:21:33'),
(2, 'https://youtu.be/Bwyj-MgrJEU', 'video 2', '2021-08-20 12:22:10', '2021-08-20 12:22:10'),
(3, 'https://youtu.be/BkssKETfnPM', 'video 3', '2021-08-20 12:22:28', '2021-08-20 12:22:28'),
(4, 'https://youtu.be/aVK_VY3GnI0', 'video 4', '2021-08-31 07:27:31', '2021-08-31 07:27:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `medias`
--
ALTER TABLE `medias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_index` (`category_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `youtube`
--
ALTER TABLE `youtube`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `medias`
--
ALTER TABLE `medias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `youtube`
--
ALTER TABLE `youtube`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
