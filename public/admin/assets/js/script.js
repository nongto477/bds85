// Image Preview
const thumbnail = document.getElementById("thumbnail");
const previewContainer = document.getElementById("imagePreview");
const previewImage = previewContainer.querySelector(".image-preview__image");
const previewDefaultText = previewContainer.querySelector(".image-preview__default-text");

thumbnail.addEventListener("change",function(){
    const file = this.files[0];

    if(file && file.size < 2097152){
        const reader = new FileReader();
        previewDefaultText.style.display = "none";
        previewImage.style.display = "block";
        reader.addEventListener("load",function(){
            previewImage.setAttribute("src",this.result);
        });
        reader.readAsDataURL(file);
    } else{
        document.getElementById("error").innerHTML = "Kích thước hình ảnh không được vượt quá 2MB";
        previewDefaultText.style.display = null;
        previewImage.style.display = null
        previewImage.setAttribute("src","");
    }
});