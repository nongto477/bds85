<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Client
Route::group(['namespace'=>'Client'],function(){
    // Homepage
    Route::get('/','HomeController@index')->name('home');
    // Article category
    Route::get('article/category/{id}/{slug}','HomeController@showArticleCategory')->name('article.category');
    // Article detail
    Route::get('article/view/{id}/{slug}','HomeController@showArticleDetail')->name('article.detail');
    // Product
    Route::get('product','HomeController@showProducts')->name('client.products');
    // Project detail
    Route::get('project/view/{id}/{slug}','HomeController@showProjectDetail')->name('project.detail');
    // Project category
    Route::get('project/category/{id}/{slug}','HomeController@showProjectCategory')->name('project.category');
    // Product detail
    Route::get('product/view/{id}/{slug}','HomeController@showProductDetail')->name('product.detail');
    // Contact
    Route::get('contact','HomeController@showContactForm')->name('contact');
    // Introduce
    Route::get('introduce/{id}/{slug}','HomeController@showIntroduceDetail')->name('introduce.detail');
    // Search
    Route::get('search', 'HomeController@search')->name('search');
    Route::get('/ql', 'HomeController@ql')->name('ql');
    Route::get('product/category/{id}','HomeController@showProductCategory')->name('product.category');
    // Youtube
    Route::get('youtube','HomeController@showYoutube')->name('youtube');
});

// Admin
Route::group(['prefix'=>'ad','namespace'=>'Admin'],function() {
    Route::get('/', function() {
        if(Auth::check()) {
            return redirect()->route('banner.list');
        } else {
            return redirect()->route('login');
        }
    });
    // Login
    Route::get('login', 'AdminController@index')->name('login');
    Route::post('login', 'AdminController@login')->name('handle.login');
    // Logout
    Route::get('logout', 'AdminController@logout')->name('handle.logout');

    // Category
    Route::group(['prefix'=>'category'],function(){
        Route::get('list','CategoryController@index')->name('category.list');
        Route::get('edit/{id}','CategoryController@edit')->name('category.edit.form');
		Route::post('edit/{id}','CategoryController@update')->name('category.edit');
        Route::get('create','CategoryController@create')->name('category.create.form');
        Route::post('create','CategoryController@store')->name('category.create');
        Route::get('delete/{id}','CategoryController@destroy')->name('category.delete');
    });

    // Banner
    Route::group(['prefix'=>'banner'],function(){
        Route::get('list','BannerController@index')->name('banner.list');
        Route::get('edit/{id}','BannerController@edit')->name('banner.edit.form');
		Route::post('edit/{id}','BannerController@update')->name('banner.edit');
        Route::get('create','BannerController@create')->name('banner.create.form');
        Route::post('create','BannerController@store')->name('banner.create');
        Route::get('delete/{id}','BannerController@destroy')->name('banner.delete');
    });

    // Product
    Route::group(['prefix'=>'product'],function(){
        Route::get('list','ProductController@index')->name('product.list');
        Route::get('edit/{sku}','ProductController@edit')->name('product.edit.form');
		Route::post('edit/{sku}','ProductController@update')->name('product.edit');
        Route::get('create','ProductController@create')->name('product.create.form');
        Route::post('create','ProductController@store')->name('product.create');
        Route::get('delete/{sku}','ProductController@destroy')->name('product.delete');
        Route::get('search','ProductController@search')->name('product.search');
    });

    // Article
    Route::group(['prefix'=>'article'],function(){
        Route::get('list','ArticleController@index')->name('article.list');
        Route::get('edit/{id}','ArticleController@edit')->name('article.edit.form');
		Route::post('edit/{id}','ArticleController@update')->name('article.edit');
        Route::get('create','ArticleController@create')->name('article.create.form');
        Route::post('create','ArticleController@store')->name('article.create');
        Route::get('delete/{id}','ArticleController@destroy')->name('article.delete');
        Route::get('search','ArticleController@search')->name('article.search');
    });

    // Project
    Route::group(['prefix'=>'project'],function(){
        Route::get('list','ProjectController@index')->name('project.list');
        Route::get('edit/{id}','ProjectController@edit')->name('project.edit.form');
		Route::post('edit/{id}','ProjectController@update')->name('project.edit');
        Route::get('create','ProjectController@create')->name('project.create.form');
        Route::post('create','ProjectController@store')->name('project.create');
        Route::get('delete/{id}','ProjectController@destroy')->name('project.delete');
        Route::get('search','ProjectController@search')->name('project.search');
    });

    Route::post('image/upload', 'ImageController@upload')->name('ckeditor.upload');
    Route::get('contact', 'ContactController@index')->name('admin.contact');
    Route::post('contact/update', 'ContactController@update')->name('admin.contact.update');
    Route::get('youtube', 'YoutubeController@index')->name('admin.youtube');
    Route::post('youtube/create', 'YoutubeController@create')->name('admin.youtube.create');
    Route::get('youtube/edit/{id}', 'YoutubeController@edit')->name('admin.youtube.edit');
    Route::post('youtube/update/{id}', 'YoutubeController@update')->name('admin.youtube.update');
    Route::get('youtube/remove/{id}', 'YoutubeController@destroy')->name('admin.youtube.remove');

});