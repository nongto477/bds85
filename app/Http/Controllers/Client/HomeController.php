<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Admin\ContactController;
use App\Models\Article;
use App\Models\Product;
use App\Models\Project;
use App\Models\Banner;
use App\Models\Contact;
use App\Models\Youtube;
use App\Models\Category;

class HomeController extends Controller
{
    public function __construct() {
        $banners = Banner::all();
        $contact = Contact::all()->first();
        if($contact == null) {
            $contact = Contact::create(ContactController::$initData);
        }
        $links = Youtube::all();
        $banggia = Article::where("type_id", "=", "3")->get();
        $categories = Category::all();
        View::share('banners', $banners);
        View::share('contact', $contact);
        View::share('links', $links);
        View::share('banggia', $banggia);
        View::share('categories', $categories);
    }
    
    /**
     * Display admin login page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $ids = [];
        $projects = Project::all();
        $leftKnow = Article::where('type_id','=',0)->take(2)->get();
        foreach($leftKnow as $item) {
            $ids[] = $item['id'];
        }
        $rightKnow = Article::whereNotIn('id', $ids)->where('type_id', '=', 0)->take(4)->get();
        $banners = Banner::all();
        return view('client.home', [
            'projects' => $projects,
            'leftKnow' => $leftKnow, 
            'rightKnow' => $rightKnow,
            'banners' => $banners
        ]);
    }

    /**
     * Display article category
     *
     * @return \Illuminate\Http\Response
     * @param  int  $id
     */
    public function showArticleCategory($id) {
        $articles = Article::where('type_id', '=', $id)->paginate(15, ['*'], 'article');
        $contrs = Project::where('type_id', '=', 4)->orderBy('view', 'DESC')->take(5)->get();
        $knowledges = Article::where('type_id', '=', 0)->orderBy('view', 'DESC')->take(5)->get();
        switch ($id) {
            case "0":
                $title = __('Kiến thức');
                break;
            case "1":
                $title = __('Giới thiệu');
                break;
            case "2":
                $title = __('Tin tức');
                break;
            case "3":
                $title = __('Bảng giá');
                break;
        }
        return view('client.article.category.list',
            [
                'articles' => $articles,
                'title' => $title,
                'contrs' => $contrs,
                'knowledges' => $knowledges
            ]
        );
    }
    
    public function showProducts() {
        $products = Product::with("image")->paginate(15, ['*'], 'product');
        $contrs = Project::where('type_id','=',4)->orderBy('view','DESC')->take(5)->get();
        $knowledges = Article::where('type_id','=',0)->orderBy('view','DESC')->take(5)->get();

        return view('client.product.list', [
            'products' => $products, 
            'contrs' => $contrs,
            'knowledges' => $knowledges
        ]);
    }

    /**
     * Display contact form
     *
     * @return \Illuminate\Http\Response
     */
    public function showContactForm() {
        return view('client.contact');
    }

    public function search(Request $request) {
        if(!isset($request->all()["q"])) {
            return redirect('/');
        }
        $query = $request->all()["q"];
        if(strlen(trim($query)) == 0) {
            return redirect('/');
        }
        
        $articles = Article::where("title", "like", "%".$query."%")
            ->orWhere("description", "like", "%".$query."%")->paginate(15, ['*'], 'article');
        $projects = Project::where("title", "like", "%".$query."%")
            ->orWhere("description", "like", "%".$query."%")->paginate(15, ['*'], 'project');
        $products = Product::with('image')
            ->where("title", "like", "%".$query."%")
            ->orWhere("description", "like", "%".$query."%")
            ->paginate(15, ['*'], 'product');

        $contrs = Project::where('type_id','=',4)->orderBy('view','DESC')->take(5)->get();
        $knowledges = Article::where('type_id','=',0)->orderBy('view','DESC')->take(5)->get();

        return view("client.search", [
            "articles" => $articles,
            "products" => $products,
            "projects" => $projects,
            "title" => "Tìm kiếm: ".$query,
            'contrs' => $contrs,
            'knowledges' => $knowledges
        ]);
    }

    public function showIntroduceDetail($id) {
        $contrs = Project::where('type_id','=',4)->orderBy('view','DESC')->take(5)->get();
        $knowledges = Article::where('type_id','=',0)->orderBy('view','DESC')->take(5)->get();
        $article = Article::where('type_id','=',$id)->get();
        return view('client.article.view.detail',
            [
                'article' => count($article) > 0 ? $article[0] : null,
                'contrs' => $contrs,
                'knowledges' => $knowledges
            ]
        );
    }

    public function showArticleDetail($id) {
        $article = Article::find($id);
        $relationArticle = Article::inRandomOrder()->where([['type_id','=',$article['type_id']], ['id','<>',$id]])->limit(8)->get();
        $contrs = Project::where('type_id','=',4)->orderBy('view','DESC')->take(5)->get();
        $knowledges = Article::where('type_id','=',0)->orderBy('view','DESC')->take(5)->get();

        // increment view
        Event::dispatch('posts.view', $article);

        return view('client.article.view.detail',
            [
                'article' => $article,
                'contrs' => $contrs,
                'knowledges' => $knowledges,
                'relationArticle' => $relationArticle
            ]
        );
    }

    public function showProductDetail($id) {
        $product = Product::with("image")->find($id);
        $title = $product['title'];
        $contrs = Project::where('type_id', '=', 4)->orderBy('view','DESC')->take(5)->get();
        $knowledges = Article::where('type_id', '=', 0)->orderBy('view','DESC')->take(5)->get();

        // increment view
        Event::dispatch('posts.view', $product);

        return view('client.product.view.detail',
            [
                "title" => $title, 'product' => $product, 
                "contrs" => $contrs, "knowledges" => $knowledges
            ]);
    }

    public function showProjectDetail($id) {
        $project = Project::with("image")->find($id);
        $relationProject = Project::inRandomOrder()->where([['type_id', '=', $project['type_id']], ['id','<>',$id]])->limit(8)->get();
        $contrs = Project::where('type_id','=',4)->orderBy('view','DESC')->take(5)->get();
        $knowledges = Article::where('type_id','=',0)->orderBy('view','DESC')->take(5)->get();

        // increment view
        Event::dispatch('posts.view', $project);

        return view('client.project.view.detail',
            [
                'project' => $project,
                'contrs' => $contrs,
                'knowledges' => $knowledges,
                'relationProject' => $relationProject
            ]
        );
    }

    /**
     * Display project category
     *
     * @return \Illuminate\Http\Response
     * @param  int  $id
     */
    public function showProjectCategory($id) {
        $projects = Project::where('type_id','=',$id)->paginate(15, ['*'],'project');
        $contrs = Project::where('type_id','=',4)->orderBy('view','DESC')->take(5)->get();
        $knowledges = Article::where('type_id','=',0)->orderBy('view','DESC')->take(5)->get();
        switch ($id) {
            case "0":
                $title = __('Biệt thự');
                break;
            case "1":
                $title = __('Biệt thự trệt');
                break;
            case "2":
                $title = __('Nhà phố');
                break;
            case "3":
                $title = __('Nhà cấp 4');
                break;
            case "4":
                $title = __('Công trình thi công');
                break;
            case "5":
                $title = __('Nội thất');
                break;
        }

        return view('client.project.category.list',
            [
                'projects' => $projects,
                'title' => $title,
                'contrs' => $contrs,
                'knowledges' => $knowledges
            ]
        );
    }

    public function ql(Request $request) {
        if(!$request->expectsJson()) {
            return redirect()->route('article.list');
        } else {
            return redirect()->route('login');
        }
    }

    /**
     * Display product category
     *
     * @return \Illuminate\Http\Response
     * @param  int  $id
     */
    public function showProductCategory($id) {
        $products = Product::with("image")->join('categories','categories.id','=','products.category_id')->where('products.category_id','=',$id)
        ->paginate(15, ['products.*','categories.name AS cate_title'], 'product-category');
        $contrs = Project::where('type_id','=',4)->orderBy('view','DESC')->take(5)->get();
        $knowledges = Article::where('type_id','=',0)->orderBy('view','DESC')->take(5)->get();

        return view('client.product.category.list', [
            'products' => $products, 
            'contrs' => $contrs,
            'knowledges' => $knowledges
        ]);
    }

    public function showYoutube()
    {
        $videos = Youtube::all();
        $contrs = Project::where('type_id','=',4)->orderBy('view','DESC')->take(5)->get();
        $knowledges = Article::where('type_id','=',0)->orderBy('view','DESC')->take(5)->get();
        return view('client.youtube.list', [
            'videos' => $videos, 
            'contrs' => $contrs,
            'knowledges' => $knowledges
        ]);
    }
}
