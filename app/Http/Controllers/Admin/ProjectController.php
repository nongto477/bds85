<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Media;
use Illuminate\Support\Facades\Storage;
use \Datetime;
use Image;

class ProjectController extends Controller {
    public function milliseconds() {
        $mt = explode(' ', microtime());
        return ((int)$mt[1]) * 1000 + ((int)round($mt[0] * 1000));
    }

    public function createName($slug, $extension) {
        return 'project_'.$slug.'_'.$this->milliseconds().'.'.$extension;
    }
    
    public function index() {
        $projects = Project::all();
        return view('admin.project.list', ['projects' => $projects]);
    }
    
    public function create() {
        return view('admin.project.create');
    }
    
    public function store(Request $request) {
        $project = Project::create([
            'title'  => $request->input('title'),
            'seo_title' => $request->input('seo-title'),
            'keyword'  => !empty($request->input('keyword')) ? $request->input('keyword') : '',
            'key_description'  => !empty($request->input('key-description')) ? $request->input('key-description') : '',
            'slug' => str_slug($request->input('title')),
            'description' => !empty($request->input('description')) ? $request->input('description') : '',
            'content' => $request->input('cnt'),
            'type_id' => $request->input('type_id'),
            'address' => $request->input('address'),
            'area' => $request->input('area'),
            'number_floor' => $request->input('number_floor'),
            'fee' => $request->input('fee'),
            'service' => $request->input('service'),
            'year_complete' => $request->input('year_complete'),
            'view' => $request->input('view')
        ]);
        if($request->has('thumbnail')) {
            foreach($request->thumbnail as $image) {
                $name = $this->createName(str_slug($request->input('title')), 'jpg');
                $image = Image::make($image->getRealPath())->encode("jpg");
                Storage::put("images/projects/$name", $image);
                $x400_image = $image->resize(400, null, function ($constraint) {$constraint->aspectRatio();})->encode("jpg");
                Storage::put("images/projects/x400/$name", $x400_image);
                $project->image()->create(["image_src" => $name]);
            }
        }
        return redirect()->route('project.list')->with("success", "Lưu thành công");
    }
    
    public function edit($id) {
        $project = Project::with('image')->find($id);
        return view('admin.project.edit',
            ['project' => $project, 'data' => $project, 'id' => $id]);
    }
    
    public function update(Request $request, $id) {
        $project = Project::find($id);
        if(is_null($project)) {
            return redirect()->route("project.edit.form", ["id" => $id])
                ->with("invalid", "Dự án không tồn tại");
        }
        $project->title = $request->input('title');
        $project->seo_title = $request->input('seo-title');
        $project->keyword = !empty($request->input('keyword')) ? $request->input('keyword') : '';
        $project->key_description = !empty($request->input('key-description')) ? $request->input('key-description') : '';
        $project->description = !empty($request->input('description')) ? $request->input('description') : '';
        $project->slug = str_slug($request->input('title'));
        $project->content = $request->input('cnt');
        $project->type_id = $request->input('type_id');
        $project->address = $request->input('address');
        $project->area = $request->input('area');
        $project->number_floor = $request->input('number_floor');
        $project->fee = $request->input('fee');
        $project->service = $request->input('service');
        $project->year_complete = $request->input('year_complete');
        $project->view = $request->input('view');
        // image progressing
        $delete_images_src = [];
        if($request->has('thumbnail_src')) {
            foreach($project->image as $project_thumbnail_src) {
                $is_delete = true;
                foreach($request->thumbnail_src as $request_thumbnail_src) {
                    if(trim($project_thumbnail_src->image_src) == trim($request_thumbnail_src)) {
                        $is_delete = false;
                        break;
                    }
                }
                if($is_delete) {
                    array_push($delete_images_src, $project_thumbnail_src->image_src);
                }
            }
        } else {
            foreach($project->image as $project_thumbnail_src) {
                array_push($delete_images_src, $project_thumbnail_src->image_src);
            }
        }
        Media::whereIn("image_src", $delete_images_src)->delete();
        foreach($delete_images_src as $project_thumbnail_src) {
            Storage::delete("images/projects/$project_thumbnail_src");
            Storage::delete("images/projects/x400/$project_thumbnail_src");
        }
        if($request->has('thumbnail')) {
            foreach($request->thumbnail as $image) {
                $name = $this->createName(str_slug($request->input('title')), 'jpg');
                $image = Image::make($image->getRealPath())->encode("jpg");
                Storage::put("images/projects/$name", $image);
                $x400_image = $image->resize(400, null, function ($constraint) {$constraint->aspectRatio();})->encode("jpg");
                Storage::put("images/projects/x400/$name", $x400_image);
                $project->image()->create(["image_src" => $name]);
            }
        }
        $project->save();
        return redirect()->route('project.list')->with("success", "Cập nhật thành công");
    }
    
    public function destroy($id) {
        $project = Project::find($id);
        foreach($project->image as $project_thumbnail) {
            Storage::delete("images/projects/$project_thumbnail->image_src");
            Storage::delete("images/projects/x400/$project_thumbnail->image_src");
        }
        $project->image()->delete();
        $project->delete();
        return redirect()->route('project.list')->with("success", "Xóa thành công");
    }

    public function search(Request $request)
    {
        $projects = Project::where('type_id',$request->input('q'))->get();
        return view('admin.project.search', ['projects' => $projects, 'q' => $request->input('q')]);
    }
}
