<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Youtube;

class YoutubeController extends Controller {

    public function index() {
        $links = Youtube::all();
        return view('admin.youtube', ['links' => $links]);
    }
    
    public function create(Request $request) {
        if(!$request->has(['link', 'title'])) {
            return redirect()->route('admin.youtube')->with("invalid", "Thiếu thông tin");
        }

        preg_match('/(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/\s]{11})/', 
            $request->input("link"), $output_array);

        if(!(count($output_array) > 1)) {
            return redirect()->route('admin.youtube')->with("invalid", "Link không đúng định dạng");
        }
        if(strlen(trim($request->input('title'))) == 0) {
            return redirect()->route('admin.youtube')->with("invalid", "Tiêu đề không được trống");
        }

        $link = Youtube::create([
            "link" => $request->input('link'),
            "title" => $request->input('title')
        ]);
        return redirect()->route('admin.youtube')->with("success", "Thêm thành công");
    }

    public function edit(Request $request, $id) {
        $link = Youtube::find($id);
        if($link == null) {
            return redirect()->route('admin.youtube')->with("invalid", "Không tìm thấy thông tin");
        }
        return view('admin.youtubeupdate', ['link' => $link, 'id' => $id]);
    }

    public function update(Request $request, $id) {
        if(!$request->has(['link', 'title'])) {
            return redirect()->route('admin.youtube')->with("invalid", "Thiếu thông tin");
        }

        $link = Youtube::find($id);
        if($link == null) {
            return redirect()->route('admin.youtube')->with("invalid", "Không tìm thấy thông tin");
        }

        preg_match('/(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/\s]{11})/', 
            $request->input("link"), $output_array);

        if(!(count($output_array) > 1)) {
            return redirect()->route('admin.youtube')->with("invalid", "Link không đúng định dạng");
        }
        if(strlen(trim($request->input('title'))) == 0) {
            return redirect()->route('admin.youtube')->with("invalid", "Tiêu đề không được trống");
        }

        $link->link = $request->input('link');
        $link->title = $request->input('title');

        $link->save();
        return redirect()->route('admin.youtube')->with("success", "Cập nhật thành công");
    }

    public function destroy($id) {
        $link = Youtube::find($id);
        if($link == null) {
            return redirect()->route('admin.youtube')->with("invalid", "Không tìm thấy link cần xóa");
        }
        $link->delete();
        return redirect()->route('admin.youtube')->with("success", "Xóa thành công");    
    }
}