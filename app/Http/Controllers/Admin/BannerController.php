<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use Illuminate\Support\Facades\Storage;
use Image;

class BannerController extends Controller {
    public function milliseconds() {
        $mt = explode(' ', microtime());
        return ((int)$mt[1]) * 1000 + ((int)round($mt[0] * 1000));
    }

    public function createName($extension) {
        return 'banner_'.$this->milliseconds().'.'.$extension;
    }
    
    public function index() {
        $banners = Banner::all();
        return view('admin.banner.list', ['banners' => $banners]);
    }
    
    public function create() {
        return view('admin.banner.create');
    }
    
    public function store(Request $request) {
        if ($request->hasFile('thumbnail')) {
            if ($request->file('thumbnail')->isValid()) {
                $name = $this->createName('jpg');
                $image = Image::make($request->file('thumbnail')->getRealPath())->encode("jpg");
                Storage::put("images/banners/$name", $image);
                $x1200_image = $image->resize(1200, null, function ($constraint) {$constraint->aspectRatio();})->encode("jpg");
                Storage::put("images/banners/x1200/$name", $x1200_image);
                
                Banner::create(['thumbnail' => $name]);
                return redirect()->route('banner.list')->with("success", "Lưu thành công");
            }
        } else {
            return view('admin.banner.create');
        }
    }
    
    public function edit($id) {
        $banner = Banner::find($id);
        return view('admin.banner.edit', ['banner' => $banner]);
    }
    
    public function update(Request $request, $id) {
        $banner = Banner::find($id);
        if ($request->hasFile('thumbnail')) {
            if ($request->file('thumbnail')->isValid()) {
                $name = $this->createName('jpg');
                $image = Image::make($request->file('thumbnail')->getRealPath())->encode("jpg");
                Storage::put("images/banners/$name", $image);
                $x1200_image = $image->resize(1200, null, function ($constraint) {$constraint->aspectRatio();})->encode("jpg");
                Storage::put("images/banners/x1200/$name", $x1200_image);
                
                Storage::delete("images/banners/{$banner['thumbnail']}");
                Storage::delete("images/banners/x1200/{$banner['thumbnail']}");

                $banner->thumbnail = $name;
            }
        }
        $banner->save();
        return redirect()->route('banner.list')->with("success", "Cập nhật thành công");
    }
    
    public function destroy($id) {
        $banner = Banner::find($id);
        Storage::delete('images/banners/'.$banner['thumbnail']);
        Storage::delete('images/banners/x1200/'.$banner['thumbnail']);
        $banner->delete();
        return redirect()->route('banner.list')->with("success", "Xóa thành công");
    }
}
