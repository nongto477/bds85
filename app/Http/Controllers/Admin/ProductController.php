<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Media;
use Illuminate\Support\Facades\Storage;
use \Datetime;
use Image;

class ProductController extends Controller
{
    public function milliseconds() {
        $mt = explode(' ', microtime());
        return ((int)$mt[1]) * 1000 + ((int)round($mt[0] * 1000));
    }

    public function createName($slug, $extension) {
        return 'product_'.$slug.'_'.$this->milliseconds().'.'.$extension;
    }
    
    public function index() {
        $products = Product::join('categories', 'categories.id', '=', 'products.category_id')
            ->get(['products.*', 'categories.name AS category_name']);
        $categories = Category::all();
        return view('admin.product.list', ['products' => $products, 'categories' => $categories]);
    }
    
    public function create() {
        $categories = Category::all();
        return view('admin.product.create', ['categories' => $categories]);
    }
    
    public function store(Request $request) {
        $check = Product::where('sku', '=', $request->input('sku'))->first();
        if(is_null($check)) {
            $product = Product::create([
                'title'  => $request->input('title'),
                'seo_title' => $request->input('seo-title'),
                'keyword'  => !empty($request->input('keyword')) ? $request->input('keyword') : '',
                'key_description'  => !empty($request->input('key-description')) ? $request->input('key-description') : '',
                'sku'   => $request->input('sku'),
                'price' => $request->input('price'),
                'slug' => str_slug($request->input('title')),
                'description' => !empty($request->input('description')) ? $request->input('description') : '',
                'content' => !empty($request->input('cnt')) ? $request->input('cnt') : '',
                'category_id' => $request->input('category'),
                'view' => $request->input('view')
            ]);
            if($request->has('thumbnail')) {
                foreach($request->thumbnail as $image) {
                    $name = $this->createName(str_slug($request->input('title')), 'jpg');
                    $image = Image::make($image->getRealPath())->encode("jpg");
                    Storage::put("images/products/$name", $image);
                    $x400_image = $image->resize(400, null, function ($constraint) {$constraint->aspectRatio();})->encode("jpg");
                    Storage::put("images/products/x400/$name", $x400_image);
                    $product->image()->create(["image_src" => $name]);
                }
            }
        } else {
            return redirect()->route('product.create')->with("invalid", "Mã sản phẩm đã tồn tại");
        }
        return redirect()->route('product.list')->with("success", "Lưu thành công");
    }
    
    public function edit($sku) {
        $product = Product::where('sku', '=', $sku)->get();
        $data = Product::find($product[0]['id']);
        $categories = Category::all();
        return view('admin.product.edit', [
            'product' => $product, 
            'data' => $data, 
            'categories' => $categories, 
            'sku' => $sku
        ]);
    }
    
    public function update(Request $request, $sku) {
        // validate
        $product = Product::with("image")->where('sku', '=', $sku)->first();
        if(is_null($product)) {
            redirect()->route('product.list')->with("invalid", "Sản phẩm không tồn tại");
        }
        $check_product_sku = Product::where('sku', '=', $request->input('sku'))->first();
        if(!is_null($check_product_sku) && $check_product_sku->id != $product->id) {
            redirect()->route('product.edit.form', ['sku' => $sku])
                ->with("invalid", "Mã sản phẩm đã tồn tại");
        }
        // update
        $product->sku = $request->input('sku');
        $product->title = $request->input('title');
        $product->seo_title = $request->input('seo-title');
        $product->keyword = !empty($request->input('keyword')) ? $request->input('keyword') : '';
        $product->key_description = !empty($request->input('key-description')) ? $request->input('key-description') : '';
        $product->price = $request->input('price');
        $product->slug = str_slug($request->input('title'));
        $product->description = !empty($request->input('description')) ? $request->input('description') : '';
        $product->content = !empty($request->input('cnt')) ? $request->input('cnt') : '';
        $product->category_id = $request->input('category');
        $product->view = $request->input('view');
        // image progressing
        $delete_images_src = [];
        if($request->has('thumbnail_src')) {
            foreach($product->image as $product_thumbnail_src) {
                $is_delete = true;
                foreach($request->thumbnail_src as $request_thumbnail_src) {
                    if(trim($product_thumbnail_src->image_src) == trim($request_thumbnail_src)) {
                        $is_delete = false;
                        break;
                    }
                }
                if($is_delete) {
                    array_push($delete_images_src, $product_thumbnail_src->image_src);
                }
            }
        } else {
            foreach($product->image as $product_thumbnail_src) {
                array_push($delete_images_src, $product_thumbnail_src->image_src);
            }
        }
        Media::whereIn("image_src", $delete_images_src)->delete();
        foreach($delete_images_src as $product_thumbnail_src) {
            Storage::delete("images/products/$product_thumbnail_src");
            Storage::delete("images/products/x400/$product_thumbnail_src");
        }
        if($request->has('thumbnail')) {
            foreach($request->thumbnail as $image) {
                $name = $this->createName(str_slug($request->input('title')), 'jpg');
                $image = Image::make($image->getRealPath())->encode("jpg");
                Storage::put("images/products/$name", $image);
                $x400_image = $image->resize(400, null, function ($constraint) {$constraint->aspectRatio();})->encode("jpg");
                Storage::put("images/products/x400/$name", $x400_image);
                $product->image()->create(["image_src" => $name]);
            }
        }

        $product->save();
        return redirect()->route('product.list')->with("success", "Cập nhật thành công");
    }
    
    public function destroy($sku) {
        $product = Product::with("image")->where('sku', '=', $sku)->first();
        foreach($product->image as $product_thumbnail) {
            Storage::delete("images/products/$product_thumbnail->image_src");
            Storage::delete("images/products/x400/$product_thumbnail->image_src");
        }
        $product->image()->delete();
        $product->delete();
        return redirect()->route('product.list')->with("success", "Xóa thành công");
    }

    public function search(Request $request)
    {
        $products = Product::where('category_id',$request->input('q'))->join('categories', 'categories.id', '=', 'products.category_id')
            ->get(['products.*', 'categories.name AS category_name']);
        $categories = Category::all();
        $category = Category::where('id',$request->input('q'))->get('name');
        return view('admin.product.search', ['products' => $products, 'categories' => $categories, 'category' => $category[0], 'q' => $request->input('q')]);
    }
}
