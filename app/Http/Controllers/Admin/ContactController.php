<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\Contact;
use Image;

class ContactController extends Controller {
    public function milliseconds() {
        $mt = explode(' ', microtime());
        return ((int)$mt[1]) * 1000 + ((int)round($mt[0] * 1000));
    }

    public function createName($extension) {
        return 'favicon_'.$this->milliseconds().'.'.$extension;
    }

    public static $initData = [
        "name_company" => "Tên công ty",
        "address" => "Địa chỉ công ty",
        "phone" => "01234567890",
        "phone_sale" => "01234567890",
        "email" => "admin@gmail.com",
        "name_contact" => "Tên người liên hệ",
        "phone_contact" => "01234567890",
        "zalo" => "01234567890",
        "youtube" => "#",
        "google_map" => "#",
        "desc_info" => "Ngay từ ngày đầu thành lập, Công ty DVT đã thực hiện các dịch vụ thiết kế và xây dựng trên nhiều địa phương khắp cả nước. Trên suốt chặng đường qua, Công ty DVT đã luôn tạo được dấu ấn tốt cho các khách hàng, không chỉ về mặt chuyên môn, mà còn gắn kết với khách hàng trong từng dự án, đảm bảo sự hài lòng và đạt được sự tín nhiệm lâu dài.",
        "desc_video_projects" => "Các Video dự án được Công ty DVT đầu tư tư chuyên nghiệp và tỉ mỉ để Quý khách hàng có cái nhìn tổng quan hơn về từng dự án.",
        "title" => "Tên trang web",
        "desc" => "Mô tả trang web",
        "keywords" => "thiết kế, nội thất, nhà đất",
        "favicon" => ""
    ];

    public function index() {
        $contact = Contact::all()->first();
        if($contact == null) {
            $contact = Contact::create(ContactController::$initData);
        }
        return view('admin.contact', ['contact' => $contact]);
    }

    public function update(Request $request) {
        $contact = Contact::all()->first();
        if(!$request->has([
            'name_company', 'address', 'phone', 'phone_sale', 'email',
            'name_contact', 'phone_contact', 'zalo', 'youtube', 'google_map',
            'desc_info', 'desc_video_projects', 'title', 'desc',
            'keywords', 'favicon'
            ])) {
            return redirect()->route('admin.contact')->with("invalid", "Thiếu thông tin");
        }
        $contact->address = $request->input('address');
        $contact->phone = $request->input('phone');
        $contact->phone_sale = $request->input('phone_sale');
        $contact->email = $request->input('email');
        $contact->name_company = $request->input('name_company');
        $contact->phone_contact = $request->input('phone_contact');
        $contact->name_contact = $request->input('name_contact');
        $contact->zalo = $request->input('zalo');
        $contact->youtube = $request->input('youtube');
        $contact->google_map = $request->input('google_map');
        $contact->title = $request->input('title');
        $contact->desc = $request->input('desc');
        $contact->keywords = $request->input('keywords');
        $contact->desc_info = $request->input('desc_info');
        $contact->desc_video_projects = $request->input('desc_video_projects');
        
        if($request->has('favicon')) {
            $name = $this->createName('jpg');
            $image = $request->file("favicon");
            $image = Image::make($image->getRealPath())->encode("jpg");
            Storage::put("images/favicons/$name", $image);
            $x16_image = $image->resize(16, null, function ($constraint) {$constraint->aspectRatio();})->encode("jpg");
            Storage::put("images/favicons/x16/$name", $x16_image);
            Storage::delete("images/favicons/$contact->favicon");
            Storage::delete("images/favicons/x16/$contact->favicon");
            $contact->favicon = $name;
        }
        
        $contact->save();
        return redirect()->route('admin.contact')->with("success", "Cập nhật thành công");
    }
}