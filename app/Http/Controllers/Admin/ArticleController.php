<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Article;
use Illuminate\Support\Facades\Storage;
use Image;

class ArticleController extends Controller
{
    public static function milliseconds() {
        $mt = explode(' ', microtime());
        return ((int)$mt[1]) * 1000 + ((int)round($mt[0] * 1000));
    }

    public static function createName($slug, $extension) {
        return 'article_'.$slug.'_'.ArticleController::milliseconds().'.'.$extension;
    }
    
    public function index() {
        $articles = Article::all();
        return view('admin.article.list', ['articles' => $articles]);
    }
    
    public function create() {
        $check = Article::where('type_id','=',1)->get();
        return view('admin.article.create', ['check' => $check]);
    }
    
    public function store(Request $request) {
        if ($request->hasFile('thumbnail') && $request->file('thumbnail')->isValid()) {
            $slug = str_slug($request->input('title'));
            $name = ArticleController::createName($slug, "jpg");
            $request->thumbnail->storeAs('images/articles', $name);
            $image_path = $request->file('thumbnail');
            $image = Image::make($image_path);
            $x400_image = $image->resize(400, null, function ($constraint) {$constraint->aspectRatio();})
                ->encode("jpg");
            Storage::put("images/articles/x400/$name", $x400_image);

            Article::create([
                'title'  => $request->input('title'),
                'seo_title' => $request->input('seo-title'),
                'keyword'  => !empty($request->input('keyword')) ? $request->input('keyword') : '',
                'key_description'  => !empty($request->input('key-description')) ? $request->input('key-description') : '',
                'slug' => $slug,
                'description' => !empty($request->input('description')) ? $request->input('description') : '',
                'content' => $request->input('cnt'),
                'type_id' => $request->input('type_id'),
                'thumbnail' => $name,
                'view' => $request->input('view')
            ]);
        }
        return redirect()->route('article.list')->with("success", "Lưu thành công");
    }
    
    public function edit($id) {
        $article = Article::find($id);
        return view('admin.article.edit', ['article' => $article]);
    }
    
    public function update(Request $request, $id) {
        $slug = str_slug($request->input('title'));
        $article = Article::find($id);
        $article->title = $request->input('title');
        $article->seo_title = $request->input('seo-title');
        $article->keyword = !empty($request->input('keyword')) ? $request->input('keyword') : '';
        $article->key_description = !empty($request->input('key-description')) ? $request->input('key-description') : '';
        $article->description = !empty($request->input('description')) ? $request->input('description') : '';
        $article->slug = $slug;
        $article->content = $request->input('cnt');
        $article->type_id = $request->input('type_id');
        $article->view = $request->input('view');
        if ($request->hasFile('thumbnail')) {
            if ($request->file('thumbnail')->isValid()) {
                $name = ArticleController::createName($slug, "jpg");
                $request->thumbnail->storeAs('images/articles', $name);
                $image_path = $request->file('thumbnail');
                $image = Image::make($image_path);
                $x400_image = $image->resize(400, null, function ($constraint) {$constraint->aspectRatio();})
                    ->encode("jpg");
                Storage::put("images/articles/x400/$name", $x400_image);

                Storage::delete('images/articles/'.$article['thumbnail']);
                Storage::delete('images/articles/x400/'.$article['thumbnail']);

                $article->thumbnail = $name;
            }
        }
        $article->save();
        return redirect()->route('article.list')->with("success", "Cập nhật thành công");
    }
    
    public function destroy($id) {
        $article = Article::find($id);
        $article->delete();
        Storage::delete('images/articles/'.$article['thumbnail']);
        Storage::delete('images/articles/x400/'.$article['thumbnail']);
        return redirect()->route('article.list')->with("success", "Xóa thành công");
    }

    public function search(Request $request)
    {
        $articles = Article::where('type_id','=',$request->input('q'))->get();
        return view('admin.article.search', ['articles' => $articles, 'q' => $request->input('q')]);
    }
}
