<?php
namespace App\Events;

class ViewPostHandler
{
    public function handle($post) {
        $post->increment('view');
    }
}