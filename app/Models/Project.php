<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 
        'seo_title',
        'keyword',
        'key_description',
        'description',
        'view',
        'type_id',
        'content',
        'slug',
        'address',
        'area',
        'number_floor',
        'fee',
        'service',
        'year_complete'
    ];
    protected $table = "projects";

    public function image() {
        return $this->morphMany(Media::class, 'imageable');
    }
}
