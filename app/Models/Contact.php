<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name_company',
        'address',
        'phone',
        'email',
        'phone_sale',
        'name_contact',
        'phone_contact',
        'zalo',
        'google_map',
        'youtube',
        'desc_video_projects',
        'desc_info',
        'title',
        'desc',
        'keywords',
        'favicon'
    ];
    protected $table = "contact";
}

