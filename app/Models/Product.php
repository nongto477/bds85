<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'seo_title',
        'keyword',
        'key_description',
        'description',
        'sku',
        'price',
        'category_id',
        'view',
        'content',
        'slug'
    ];
    protected $table = "products";

    /**
     * Get the product's image.
     */
    public function image() {
        return $this->morphMany(Media::class, 'imageable');
    }
}
