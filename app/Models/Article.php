<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'thumbnail', 
        'title', 
        'seo_title',
        'keyword',
        'key_description',
        'description',
        'view',
        'type_id',
        'content',
        'slug'
    ];
    protected $table = "articles";
}
